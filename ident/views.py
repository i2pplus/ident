import random
from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Max
from django.views.generic import View, TemplateView

from ident.dent.models import Dent, Vote

class IdentViewMixin(object):
    """Adds some context for stuff that is displayed in the sidebar"""
    def get_context_data(self, **kwargs):
        context = super(IdentViewMixin, self).get_context_data(**kwargs)
        # Add in the number of dents and users
        num_dents = Dent.objects.count()
        context['num_dents'] = num_dents
        context['num_users'] = User.objects.count()
        context['num_up_votes'] = Vote.objects.filter(value=1).count()
        context['num_down_votes'] = Vote.objects.filter(value=-1).count()

        #fetch a dent-du-jour
        if False and num_dents:
            now = datetime.now()
            one_day = timedelta(days=1)
            try:
                # Get the current dent du jour
                context['dent_du_jour'] = Dent.objects.get(last_featured__gt=(now-one_day))
            except Dent.MultipleObjectsReturned:
                # We should never get here, but just in case...
                dent_du_jours = Dent.objects.filter(last_featured__gt=(now-one_day))
                rand = random.randint(0, dent_du_jours.count()-1)
                context['dent_du_jour'] = dent_du_jours[rand]
            except Dent.DoesNotExist:
                # Get a new dent du jour
                # This returns a random dent that hasn't been featured in the last
                # three days from all dents with a damped popularity above 0.85 (a
                # value chosen somewhat arbitrarily after examining the vote stats).
                # TODO: make this more intelligent/useful
                three_days = timedelta(days=3)
                shortlisted_dents = Dent.objects.filter(
                        damped_popularity__gt=0.85
                    ).exclude(
                        last_featured__gt=(now-three_days)
                    )
                num_shortlisted = shortlisted_dents.count()
                rand = 0
                if num_shortlisted > 1:
                    rand = random.randint(0, shortlisted_dents.count()-1)
                if num_shortlisted > 0:
                    dent_du_jour = shortlisted_dents[rand]

                    # Record this as the current dent du jour
                    dent_du_jour.last_featured = now
                    dent_du_jour.times_featured = dent_du_jour.times_featured + 1
                    dent_du_jour.save()
                    context['dent_du_jour'] = dent_du_jour

        #Fetch latest 3 IdentNews entries
        context['IdentNews'] = {
            'user': User.objects.get(username='IdentNews'),
            'dents': Dent.objects.filter(
                poster__username='IdentNews')[:3],
            }

        #get latest user
        try:
            context['latest_user'] = User.objects.latest('date_joined')
        except User.DoesNotExist:
            context['latest_user'] = None

        # get feedback admins from settings
        context['feedback_admins'] = settings.FEEDBACK_ADMINS
        return context


class IdentTemplateView(IdentViewMixin, TemplateView):
    """A Template view enriched with context for sidebar items"""
    pass

class StatisticsView(IdentViewMixin, TemplateView):
    template_name="stats.html"

    def get_context_data(self, **kwargs):
        context = super(StatisticsView, self).get_context_data(**kwargs)
        if context['num_dents']:
            context['reply_share'] = 100 *\
                Dent.objects.filter(in_reply_to__gt=0).count() /\
                    context['num_dents']

        dent_stats = [{'td':'Dents'}, {'td':'Users denting'},
                      {'td':'Users logged in'}]
        """[dents, users, logged_in] list containing values for
        td,24h,1w,total"""

        # Add in the number of dents in in lasat 24h/week
        now = datetime.now()
        one_day  = timedelta(days=1)
        dents = Dent.objects.filter(time__gt=(now-one_day))
        dent_stats[0]['1d'] = dents.count()
        dent_stats[1]['1d'] = User.objects.filter(dent__in=dents).distinct().\
            count()
        dent_stats[2]['1d'] = User.objects.filter(
            last_login__gt=(now-one_day)).count()

        one_week = timedelta(days=7)
        dents = Dent.objects.filter(time__gt=(now-one_week))
        dent_stats[0]['1w'] = dents.count()
        dent_stats[1]['1w'] = User.objects.filter(dent__in=dents).distinct().\
            count()
        dent_stats[2]['1w'] = User.objects.filter(
            last_login__gt=(now-one_week)).count()

        dents = Dent.objects.all()
        dent_stats[0]['total'] = dents.count()
        dent_stats[1]['total'] = User.objects.filter(dent__in=dents).distinct().count()
        dent_stats[2]['total'] = User.objects.count()


        context['dent_stats'] = dent_stats
        return context


class MessageView(TemplateView):
    """Shows Ident.i2p-themed page, displaying 'heading' and 'content'"""
    template_name="base.html"
    heading = ''
    content = ''

    def get_context_data(self, **kwargs):
        """Pass heading and content kwargs into template context"""
        # get original context
        context = super(MessageView, self).get_context_data(**kwargs)
        context['heading'] = self.heading
        context['content'] = self.content
        return context

def index_handler(request):
    """The index handler

    Users can configure which page they want to use as their index page in
    their profile. For non-authenticated users, the homepage is used."""
    if request.user and request.user.id:
        uss = request.user.sitesettings
        index_page = request.user.sitesettings.index_page
        if index_page == uss.IndexPages.HOMEPAGE:
            from ident.dent.views import DentListView
            return DentListView.as_view()(request)
        elif index_page == uss.IndexPages.DASHBOARD:
            from ident.user.views import UserDetailView
            return UserDetailView.as_view()(request, username=request.user.username)
        elif index_page == uss.IndexPages.SUBSCRIBED:
            from ident.dent.views import UserDentListView
            return UserDentListView.as_view()(request, username=request.user.username, stalkovision=True)
        elif index_page == uss.IndexPages.USERPAGE:
            from ident.dent.views import UserDentListView
            return UserDentListView.as_view()(request, username=request.user.username)

    # Default: return the homepage
    from ident.dent.views import DentListView
    return DentListView.as_view()(request)
