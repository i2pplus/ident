#email settings
EMAIL_HOST= 'localhost'
EMAIL_PORT= 7659
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = ""
#alternatively use the msmtp backend (Check that file to configure the actual command):
EMAIL_BACKEND = 'ident.user.sendmail_backend.EmailBackend'
