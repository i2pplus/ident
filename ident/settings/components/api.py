REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_oauth.authentication.OAuthAuthentication',
    )
}

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'oauth_provider.backends.XAuthAuthenticationBackend',
)

OAUTH_AUTHORIZE_VIEW = 'ident.api.views.oauth_authorize'
OAUTH_CALLBACK_VIEW = 'ident.api.views.oauth_verified'

OAUTH_REALM_KEY_NAME = 'http://id3nt.i2p/'

OAUTH_BLACKLISTED_HOSTNAMES = ['localhost', '127.0.0.1']

OAUTH_PROVIDER_SECRET_SIZE = 48
