DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ident_db',
        'USER': 'ident_user',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
    }
}
