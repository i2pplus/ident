# Django settings for ident project.
from django.conf import global_settings

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ABSOLUTE_URL_OVERRIDES = {
    'auth.user': lambda o: "/u/%s/" % o.username,
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'UTC'
UZE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

TRANSLATED_LANGUAGES = (
    ('en', 'English'),
    ('es', 'Spanish'),
    ('ru', 'Russian'),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = False

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = global_settings.TEMPLATE_LOADERS

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
    'ident.context_processors.translated_languages',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'ident.middleware.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'gadjo.requestprovider.middleware.RequestProvider',
)

ROOT_URLCONF = 'ident.urls'

#Log users out by default when they close their web browser
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

#registration app uses this:
ACCOUNT_ACTIVATION_DAYS = 7 # One-week activation window; you may, of course, use a different value.
LOGIN_REDIRECT_URL = '/accounts/logged_in/'
CSRF_FAILURE_VIEW = 'ident.user.views.csrf_failure'

# force removal of ident.fcgi from URL:
# http://docs.djangoproject.com/en/dev/howto/deployment/fastcgi/#forcing-the-url-prefix-to-a-particular-value
FORCE_SCRIPT_NAME = ''

MESSAGE_STORAGE = 'messages_extends.storages.FallbackStorage'

# Settings for the feedback form
FEEDBACK_ADMINS = [
    'str4d',
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'django_nuages_tag',
    'hashtags',
    'messages_extends',
    'reputation',
    'rest_framework',
    'oauth_provider',
    'ident',
   #We don't do the email confirmation thingie
   # 'registration',
   # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
)
