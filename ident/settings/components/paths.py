from os.path import join

APP_ROOT = join(PROJECT_ROOT, 'ident')

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = join(APP_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute paths to the directories that hold static files.
STATICFILES_DIRS = (
    join(APP_ROOT, 'static'),
)

# Absolute path to the directory that "manage.py collectstatic" will dump all
# static files into.
STATIC_ROOT = join(APP_ROOT, 'static-files')

# URL that handles the static files served from STATIC_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
STATIC_URL = '/static/'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    join(APP_ROOT, 'templates'),
)

# Virtualenv settings
VIRTUALENV = join(PROJECT_ROOT, 'env/lib/python2.7/site-packages')
