# Reputation app settings
REPUTATION_MAX_LOSS_PER_DAY = 2000000
REPUTATION_MAX_GAIN_PER_DAY = 2000000
REPUTATION_PERMISSONS = {
    'can_vote_up': 20,
    'can_vote_down': 40,
    'can_use_privatedents': 60,
    }
