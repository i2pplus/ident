from split_settings.tools import optional, include

include(
    'env.py',
    'components/base.py',
    'components/database.py',
    'components/paths.py',
    'components/email.py',
    'components/reputation.py',
    'components/api.py',
    'security.py',
    optional('local.py'),

    scope=locals()
)
