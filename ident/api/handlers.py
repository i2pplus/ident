from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from piston.handler import AnonymousBaseHandler, BaseHandler
from piston.utils import rc, throttle, validate

from ident.dent.forms import DentForm
from ident.dent.models import Dent

from .utils import catch_and_return

# http://domain/api/2.0
class RootHandlerAnonymous(AnonymousBaseHandler):
    allowed_methods = ('GET',)

    def read(self, request):
        return basic_root()

# http://domain/api/2.0
class RootHandler(BaseHandler):
    allowed_methods = ('GET',)
    anonymous = RootHandlerAnonymous

    def read(self, request):
        kwargs = {'username': request.user.username}
        root = basic_root()
        return root

def basic_root():
    return {
        'oauth_request_token_url': reverse('oauth_request_token'),
        'oauth_authorize_url': reverse('oauth_user_auth'),
        'oauth_access_token_url': reverse('oauth_access_token'),
        'api-version': '2.0'
    }

# http://domain/api/2.0/u
class UsersHandler(BaseHandler):
    allowed_methods = ('GET',)

    def read(self):
        """
        Returns the list of users
        """
        return User.objects.all().values_list('username', flat=True)

# http://domain/api/2.0/u/pk
# http://domain/api/2.0/u/username
class UserHandler(BaseHandler):
    allowed_methods = ('GET',)

    @catch_and_return(ObjectDoesNotExist, rc.NOT_HERE)
    def read(self, request, pk=None, username=None):
        """
        Returns a single user
        """
        users = User.objects
        if username:
	    u = users.get(username=username)
	else:
            u = users.get(pk=pk)
        p = u.profile
        obj= {}
        obj['username'] = u.username
        obj['homepage'] = p.homepage
        obj['I2PBote'] = p.i2pbote
        obj['I2PMessenger'] = p.i2pmessenger
        if p.email_is_public:
            obj['email'] = u.email
        obj['num_dents'] = p.dent_count()
        obj['last_dents'] = str(p.last_dent_time())
        obj['joined'] = str(u.date_joined.date())
        obj['karma'] = p.karma
        obj['stalking'] = list(p.subscriptions.all().values_list('username', flat=True))
        obj['stalkers'] = list(u.followers.all().values_list('user__username', flat=True))
        return obj

# http://domain/api/2.0/d
class DentsHandlerAnonymous(AnonymousBaseHandler):
    allowed_methods = ('GET',)
    model = Dent

# http://domain/api/2.0/d
class DentsHandler(BaseHandler):
    allowed_methods = ('GET', 'POST')
    anonymous = DentsHandlerAnonymous
    model = Dent

    @catch_and_return(ObjectDoesNotExist, rc.NOT_HERE)
    def read(self, request, dent_id):
        """
        Returns 30 dents
        """
        dents = Dent.objects
        return dents.all()[:30]

    @throttle(5, 10*60)
    @validate(DentForm, 'POST')
    def create(self, request):
        data = request.form.cleaned_data
        dent = self.model(text=data['text'], poster=request.user)
        dent.save()
        return rc.CREATED

# http://domain/api/2.0/d/dent_id
class DentHandler(BaseHandler):
    allowed_methods = ('GET',)
    model = Dent
    fields = ('id', 'poster', 'text', 'time', 'vote', 'in_reply_to')

    @classmethod
    def vote(self, dent):
        return dent.vote_sum

    @classmethod
    def poster(self, dent):
        return dent.poster.username

    @catch_and_return(ObjectDoesNotExist, rc.NOT_HERE)
    def read(self, request, pk):
        """
        Returns a single dent
        """
        dents = Dent.objects
        return dents.get(pk=pk)
