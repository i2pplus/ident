from django.conf.urls import include, url

urlpatterns = [
    # API v1.1
    url(r'^1.1/', include('ident.api.twitter.v1_1.urls')),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^oauth/', include('ident.api.oauth_urls')),
]
