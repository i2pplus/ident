from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.contrib.auth.models import User
from gadjo.requestprovider.signals import get_request
from pytz import UTC
from rest_framework import exceptions as rex
from rest_framework import serializers as s

from ident.api.utils import catch_and_raise
from ident.dent.models import Dent, PrivateDent, Vote
from ident.user.models import SiteSettings

def get_param(request, key):
    if request.GET.has_key(key):
        return request.GET[key]
    elif request.data.has_key(key):
        return request.data[key]
    else:
        return None


class CursorSerializer(s.Serializer):
    previous_cursor = s.IntegerField(read_only=True)
    previous_cursor_str = s.CharField(read_only=True, source='previous_cursor')
    next_cursor = s.IntegerField(read_only=True)
    next_cursor_str = s.CharField(read_only=True, source='next_cursor')


class StatusSerializer(s.Serializer):
    created_at = s.SerializerMethodField()
#    entities = s.DictField()
    favorite_count = s.IntegerField(read_only=True, source='vote_sum')
    favorited = s.SerializerMethodField()
    id = s.IntegerField(read_only=True)
    id_str = s.CharField(read_only=True, source='id')
    in_reply_to_screen_name = s.CharField(read_only=True,
        source='in_reply_to.poster.username')
    in_reply_to_status_id = s.IntegerField(required=False, allow_null=True,
        source='in_reply_to.id')
    in_reply_to_status_id_str = s.CharField(read_only=True,
        source='in_reply_to.id')
    in_reply_to_user_id = s.IntegerField(read_only=True,
        source='in_reply_to.poster.id')
    in_reply_to_user_id_str = s.CharField(read_only=True,
        source='in_reply_to.poster.id')
    lang = s.CharField(read_only=True, source='language')
    source = s.CharField(read_only=True)
    text = s.CharField(read_only=True)
    truncated = s.SerializerMethodField()

    # POST-only fields
    status = s.CharField(write_only=True, source='text')

    @classmethod
    def new(cls, request, obj=None, data=s.empty, **kwargs):
        trim = get_param(request, 'trim_user')
        trim = trim in ['true', 't', '1']
        return cls(obj, data=data, trim_user=trim, **kwargs)

    def __init__(self, *args, **kwargs):
        nested = kwargs.pop('nested', False)
        trim_user = kwargs.pop('trim_user', False)
        super(StatusSerializer, self).__init__(*args, **kwargs)
        if not nested:
            if trim_user:
                self.fields['user'] = TrimmedUserSerializer(
                    read_only=True, source='poster')
            else:
                self.fields['user'] = UserSerializer(
                    read_only=True, source='poster', skip_status='true')

    def get_created_at(self, obj):
        return obj.time.replace(tzinfo=UTC).strftime('%a %b %d %H:%M:%S %z %Y')

    def get_favorited(self, obj):
        request = get_request()
        vote = Vote.objects.filter(voter__id=request.user.id, dent=obj)
        return vote.count() == 1 and vote[0].value == 1

    def get_truncated(self, obj):
        return False

    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def create(self, validated_data):
        dent = Dent(
            text=validated_data['text'],
            poster=validated_data['poster'],
            source='Twitter API')
        if validated_data.has_key('in_reply_to'):
            replyto_id = validated_data['in_reply_to']['id']
            if replyto_id > 0:
                dent.in_reply_to = Dent.objects.get(id=replyto_id)
        dent.save()
        return dent


class TrimmedUserSerializer(s.Serializer):
    id = s.IntegerField(read_only=True)
    id_str = s.CharField(read_only=True, source='id')


class UserSerializer(TrimmedUserSerializer):
    contributors_enabled = s.SerializerMethodField()
    created_at = s.SerializerMethodField()
    default_profile = s.SerializerMethodField()
    default_profile_image = s.SerializerMethodField()
    description = s.CharField(required=False, allow_null=True,
        source='profile.description')
    favourites_count = s.SerializerMethodField()
    followers_count = s.IntegerField(read_only=True, source='followers.count')
    friends_count = s.IntegerField(read_only=True,
        source='profile.subscriptions.count')
    geo_enabled = s.SerializerMethodField()
    is_translator = s.SerializerMethodField()
    lang = s.CharField(read_only=True, source='sitesettings.language')
    listed_count = s.SerializerMethodField()
    location = s.SerializerMethodField()
    name = s.CharField(required=False, source='username')
    profile_background_color = s.SerializerMethodField()
    profile_background_image_url = s.SerializerMethodField()
    profile_background_image_url_https = s.SerializerMethodField()
    profile_background_tile = s.SerializerMethodField()
    profile_image_url = s.SerializerMethodField()
    profile_image_url_https = s.SerializerMethodField()
    profile_link_color = s.SerializerMethodField()
    profile_sidebar_border_color = s.SerializerMethodField()
    profile_sidebar_fill_color = s.SerializerMethodField()
    profile_text_color = s.SerializerMethodField()
    profile_use_background_image = s.SerializerMethodField()
    protected = s.SerializerMethodField()
    screen_name = s.CharField(read_only=True, source='username')
    show_all_inline_media = s.SerializerMethodField()
    statuses_count = s.IntegerField(read_only=True, source='profile.dent_count')
    time_zone = s.SerializerMethodField()
    url = s.URLField(required=False, source='profile.homepage')
    utc_offset = s.SerializerMethodField()
    verified = s.SerializerMethodField()

    @classmethod
    def new(cls, request, obj=None, data=s.empty, **kwargs):
        params = {key: get_param(request, key) for key in [
                'include_entities',
                'skip_status',
            ]}
        kwargs.update(params)
        return cls(obj, data=data, **kwargs)

    def __init__(self, *args, **kwargs):
        include_entities = kwargs.pop('include_entities', 'true')
        skip_status = kwargs.pop('skip_status', 'false')

        super(UserSerializer, self).__init__(*args, **kwargs)

#        if include_entities:
#            self.fields['entities' = s.DictField()
        if skip_status not in ['true', 't', '1']:
            self.fields['status'] = StatusSerializer(
                read_only=True, source='profile.last_dent', nested=True)

    def get_contributors_enabled(self, obj):
        return False

    def get_created_at(self, obj):
        return obj.date_joined.replace(tzinfo=UTC).strftime('%a %b %d %H:%M:%S %z %Y')

    def get_default_profile(self, obj):
        return True

    def get_default_profile_image(self, obj):
        return False

    def get_favourites_count(self, obj):
        return obj.vote_set.filter(value__gt=0).count()

    def get_geo_enabled(self, obj):
        return False

    def get_is_translator(self, obj):
        return False

    def get_listed_count(self, obj):
        return 0

    def get_location(self, obj):
        return 'I2P'

    def get_profile_background_color(self, obj):
        if obj.sitesettings.theme == SiteSettings.Themes.CORPORAT:
            return 'DDDDFF'
        else: # BURGUNDY
            return '220804'

    def get_profile_background_image_url(self, obj):
        request = get_request()
        if obj.sitesettings.theme == SiteSettings.Themes.CORPORAT:
            image = 'themes/corporat/images/lyudi.png'
        else: # BURGUNDY
            image = 'themes/burgundy/images/skyline.png'
        return request.build_absolute_uri('%s%s' % (settings.STATIC_URL, image))

    def get_profile_background_image_url_https(self, obj):
        return self.get_profile_background_image_url(obj)

    def get_profile_background_tile(self, obj):
        return True

    def get_profile_image_url(self, obj):
        request = get_request()
        if obj.profile.avatar:
            image = obj.profile.avatar.url
        else:
            image = '%savatars/_default.png' % settings.MEDIA_URL
        image = '_normal.'.join(image.rsplit('.', 1))
        return request.build_absolute_uri(image)

    def get_profile_image_url_https(self, obj):
        return self.get_profile_image_url(obj)

    def get_profile_link_color(self, obj):
        if obj.sitesettings.theme == SiteSettings.Themes.CORPORAT:
            return '111199'
        else: # BURGUNDY
            return 'FF6600'

    def get_profile_sidebar_border_color(self, obj):
        if obj.sitesettings.theme == SiteSettings.Themes.CORPORAT:
            return '9999FF'
        else: # BURGUNDY
            return 'EA7C21'

    def get_profile_sidebar_fill_color(self, obj):
        if obj.sitesettings.theme == SiteSettings.Themes.CORPORAT:
            return 'EEEEFF'
        else: # BURGUNDY
            return '330000'

    def get_profile_text_color(self, obj):
        if obj.sitesettings.theme == SiteSettings.Themes.CORPORAT:
            return '000033'
        else: # BURGUNDY
            return 'F5A436'

    def get_profile_use_background_image(self, obj):
        return True

    def get_protected(self, obj):
        return False

    def get_show_all_inline_media(self, obj):
        return False

    def get_time_zone(self, obj):
        return "GMT/UTC"

    def get_utc_offset(self, obj):
        return 0

    def get_verified(self, obj):
        return False

    def update(self, instance, validated_data):
        if validated_data.has_key('profile'):
            profile_data = validated_data['profile']
            p = instance.profile
            p.description = profile_data.get('description', p.description)
            p.homepage = profile_data.get('homepage', p.homepage)
            p.save()
        return instance


class UsersSerializer(CursorSerializer):
    ids = s.ListField(child=s.IntegerField(), read_only=True)

    def __init__(self, *args, **kwargs):
        include_user_entities = kwargs.pop('include_user_entities', 'true')
        skip_status = kwargs.pop('skip_status', 'false')

        super(UsersSerializer, self).__init__(*args, **kwargs)

#        if include_entities:
#            self.fields['entities' = s.DictField()
        self.fields['users'] = UserSerializer(read_only=True, many=True,
            include_entities=include_user_entities,
            skip_status=skip_status)


class StringifiedUsersSerializer(CursorSerializer):
    ids = s.ListField(child=s.CharField(), read_only=True)


class ListsSerializer(CursorSerializer):
    lists = s.ListField(child=s.CharField(), read_only=True)


class DirectMessageSerializer(s.Serializer):
    created_at = s.SerializerMethodField()
#    entities = s.DictField()
    id = s.IntegerField(read_only=True)
    id_str = s.CharField(read_only=True, source='id')
    recipient_id = s.IntegerField(read_only=True,
        source='rec_user.id')
    recipient_screen_name = s.CharField(read_only=True,
        source='rec_user.username')
    sender_id = s.IntegerField(read_only=True,
        source='poster.id')
    sender_screen_name = s.CharField(read_only=True,
        source='poster.username')
    text = s.CharField()

    # POST-only fields
    user_id = s.IntegerField(write_only=True, required=False)
    screen_name = s.CharField(write_only=True, required=False)

    @classmethod
    def new(cls, request, obj=None, data=s.empty, **kwargs):
        params = {key: get_param(request, key) for key in [
                'include_entities',
                'skip_status',
            ]}
        kwargs.update(params)
        return cls(obj, data=data, **kwargs)

    def __init__(self, *args, **kwargs):
        include_entities = kwargs.pop('include_entities', 'true')
        skip_status = kwargs.pop('skip_status', 'false')

        super(DirectMessageSerializer, self).__init__(*args, **kwargs)

#        if include_entities:
#            self.fields['entities' = s.DictField()
        self.fields['recipient'] = UserSerializer(read_only=True,
            source='rec_user',
            include_entities=include_entities,
            skip_status=skip_status)
        self.fields['sender'] = UserSerializer(read_only=True,
            source='poster',
            include_entities=include_entities,
            skip_status=skip_status)

    def get_created_at(self, obj):
        return obj.time.replace(tzinfo=UTC).strftime('%a %b %d %H:%M:%S %z %Y')

    def create(self, validated_data):
        if validated_data.has_key('user_id'):
            recipient = User.objects.get(pk=validated_data['user_id'])
        else:
            recipient = User.objects.get(username=validated_data['screen_name'])
        pdent = PrivateDent(
            text=validated_data['text'],
            poster=validated_data['poster'],
            rec_user=recipient)
        pdent.save()
        return pdent


class RelationshipTargetSerializer(TrimmedUserSerializer):
    following = s.BooleanField(read_only=True)
    followed_by = s.BooleanField(read_only=True)
    screen_name = s.CharField(read_only=True, source='username')

class RelationshipSourceSerializer(RelationshipTargetSerializer):
    can_dm = s.BooleanField(read_only=True)

class RelationshipSerializer(s.Serializer):
    source = RelationshipSourceSerializer()
    target = RelationshipTargetSerializer()

class ShowFriendshipSerializer(s.Serializer):
    relationship = RelationshipSerializer()

class RelationshipLookupSerializer(TrimmedUserSerializer):
    name = s.CharField(required=False, source='username')
    screen_name = s.CharField(read_only=True, source='username')
    connections = s.SerializerMethodField()

    def get_connections(self, obj):
        user = get_request().user
        ret = []
        if obj.profile.subscriptions.filter(id=user.id).count() > 0:
            ret.append('following')
        if user.profile.subscriptions.filter(id=obj.id).count() > 0:
            ret.append('followed_by')
        if not ret:
            ret.append('none')
        return ret


class SettingsSerializer(s.Serializer):
    lang = s.CharField(required=False, source='language')

    def update(self, instance, validated_data):
        instance.language = validated_data.get('language', instance.language)
        instance.save()
        return instance


class ProfileImageSerializer(s.Serializer):
    image = s.ImageField(write_only=True, source='avatar')

    def update(self, instance, validated_data):
        # TODO test and get working
        print validated_data
        #instance.save()
        return instance
