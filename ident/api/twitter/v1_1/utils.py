from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError

from rest_framework import exceptions as rex

from ident.api.utils import catch_and_raise


@catch_and_raise(MultiValueDictKeyError, rex.ParseError)
@catch_and_raise(ObjectDoesNotExist, rex.NotFound)
def get_user(request, default_to_auth=False):
    users = User.objects
    if request.method == 'GET':
        if request.GET.has_key('user_id'):
            return users.get(pk=request.GET['user_id'])
        elif not default_to_auth:
            return users.get(username=request.GET['screen_name'])
    else:
        if request.data.has_key('user_id'):
            return users.get(pk=request.data['user_id'])
        elif not default_to_auth:
            return users.get(username=request.data['screen_name'])
    # default_to_auth is True, return authenticated user
    return request.user
