from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError

import datetime
from rest_framework import exceptions as rex
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from ident.api.twitter.v1_1.serializers import StatusSerializer
from ident.api.twitter.v1_1.utils import get_user
from ident.api.utils import catch_and_raise
from ident.dent.models import Dent, Vote

from . import BaseStatusList


class List(BaseStatusList):
    def get_dents(self, request):
        u = get_user(request, True)
        return Dent.objects.filter(votes__id=u.id, vote__value__gt=0)


# POST favorites/destroy
class Destroy(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def post(self, request, format=None):
        dent = Dent.objects.get(pk=request.data['id'])
        user = request.user
        try:
            vote = Vote.objects.get(dent=dent, voter=user)
            if vote.value > 0:
                vote.delete()
        except Vote.DoesNotExist:
            pass

        serializer = StatusSerializer.new(request, dent)
        return Response(serializer.data)


# POST favorites/create
class Create(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def post(self, request, format=None):
        dent = Dent.objects.get(pk=request.data['id'])
        user = request.user

        sinceday = datetime.datetime.now() - datetime.timedelta(days=1)
        sinceweek = datetime.datetime.now() - datetime.timedelta(days=7)
        day_user_vote_count = Vote.objects.filter(voter__id=user.id, dent__poster=dent.poster, time__gt=sinceday).count()
        week_user_vote_count = Vote.objects.filter(voter__id=user.id, dent__poster=dent.poster, time__gt=sinceweek).count()
        if day_user_vote_count >= 3:
            raise rex.Throttled(detail=_("You've already cast 3 votes on target's dents in the last 24 hours."))
        elif week_user_vote_count >= 10:
            raise rex.Throttled(detail=_("You've already cast 10 votes on target's dents in the last week."))

        value = 1
        if value > 0 and not user.has_perm('ident.can_vote_up'):
            raise rex.PermissionDenied(detail=_("You don't have enough karma to vote dents up."))

        if user == dent.poster:
            raise rex.PermissionDenied()
        try:
            vote = Vote.objects.get(dent=dent, voter=user)
        except Vote.DoesNotExist:
            vote = Vote(dent=dent, voter=user)
        vote.value = value
        vote.save()

        serializer = StatusSerializer.new(request, dent)
        return Response(serializer.data)
