from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError

from rest_framework import exceptions as rex
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ident.api.utils import catch_and_raise, KarmaRateThrottle
from ident.dent.models import Dent

from ident.api.twitter.v1_1.serializers import *
from ident.api.twitter.v1_1.utils import get_user

from . import BaseStatusList


class BaseTimeline(BaseStatusList):
    def filter_dents(self, request, dents):
        if request.GET.has_key('exclude_replies') and request.GET['exclude_replies'] == 'true':
            dents = dents.filter(in_reply_to__isnull=True)
        return dents


# GET statuses/mentions_timeline
class MentionsTimeline(BaseTimeline):
    def get_dents(self, request):
        return Dent.objects.filter(text__contains='@'+request.user.username)


# GET statuses/user_timeline
class UserTimeline(BaseTimeline):
    def get_dents(self, request):
        u = get_user(request)
        return Dent.objects.filter(poster=u)


# GET statuses/home_timeline
class HomeTimeline(BaseTimeline):
    def get_dents(self, request):
        f = User.objects.filter(Q(id=request.user.id) | Q(followers__user__id=request.user.id))
        return Dent.objects.filter(poster__in=f)


# GET statuses/show/:id
class Show(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def get(self, request, format=None):
        """
        Returns a single dent
        """
        dent = Dent.objects.get(pk=request.GET['id'])
        serializer = StatusSerializer.new(request, dent)
        return Response(serializer.data)


# POST statuses/destroy/:id
class Destroy(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def post(self, request, pk, format=None):
        dent = Dent.objects.get(pk=pk)

        if dent.poster != request.user:
            raise rex.PermissionDenied

        dent.delete()
        serializer = StatusSerializer.new(request, dent)
        return Response(serializer.data)


# POST statuses/update
class Update(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    throttle_classes = (KarmaRateThrottle,)

    def post(self, request, format=None):
        serializer = StatusSerializer.new(request, data=request.data)
        if serializer.is_valid():
            serializer.save(poster=request.user)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# GET statuses/lookup
class Lookup(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        return self.lookup(request)

    def post(self, request, format=None):
        return self.lookup(request)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def lookup(self, request):
        dent_ids = request.GET['id'].split(',')
        dents = Dent.objects.filter(id__in=dent_ids)[:100]
        return Response(StatusSerializer.new(request, dents, many=True).data)
