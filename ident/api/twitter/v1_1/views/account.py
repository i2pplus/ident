from rest_framework import permissions, status
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from ident.api.twitter.v1_1.serializers import *


# GET account/verify_credentials
class VerifyCredentials(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        serializer = UserSerializer.new(request, request.user)
        return Response(serializer.data)


# GET account/settings
# POST account/settings
class Settings(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        serializer = SettingsSerializer(request.user.sitesettings)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SettingsSerializer(
            request.user.sitesettings, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# POST account/update_profile
class UpdateProfile(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = UserSerializer.new(request, request.user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# POST account/update_profile_image
class UpdateProfileImage(APIView):
    parser_classes = (MultiPartParser,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = ProfileImageSerializer(
            request.user.profile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            us = UserSerializer.new(request, request.user)
            return Response(us.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
