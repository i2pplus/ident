from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError

from rest_framework import exceptions as rex
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ident.api.utils import catch_and_raise, KarmaRateThrottle
from ident.dent.models import PrivateDent

from ident.api.twitter.v1_1.serializers import *


class BaseDirectMessages(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        # Get PrivateDents
        pdents = self.get_pdents(request)
        # Common filters
        if request.GET.has_key('since_id'):
            pdents = pdents.filter(id__gt=request.GET['since_id'])
        if request.GET.has_key('max_id'):
            pdents = pdents.filter(id__lte=request.GET['max_id'])
        count = 200
        if request.GET.has_key('count') and request.GET['count'] < 200:
            count = request.GET['count']
        pdents = pdents[:count]
        # Convert and return
        return Response(DirectMessageSerializer.new(request, pdents, many=True).data)


# GET direct_messages/sent
class Sent(BaseDirectMessages):
    def get_pdents(self, request):
        return PrivateDent.objects.filter(poster=request.user)


# GET direct_messages
class Received(BaseDirectMessages):
    def get_pdents(self, request):
        return PrivateDent.objects.filter(rec_user=request.user)


# GET direct_messages/show
class Show(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def get(self, request, format=None):
        pdent = PrivateDent.objects.get(pk=request.GET['id'])
        serializer = DirectMessageSerializer.new(request, pdent)
        return Response(serializer.data)


# POST direct_messages/destroy
class Destroy(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def post(self, request, format=None):
        pdent = PrivateDent.objects.get(pk=request.data['id'])

        # Permission to delete?
        can_delete = pdent.poster == request.user or \
            pdent.rec_user == request.user or \
            request.user.has_perm('ident.delete_privatedent')
        if not can_delete:
            raise rex.PermissionDenied

        # Generate response before deleting, so id is still there
        serializer = DirectMessageSerializer.new(request, pdent)
        response = Response(serializer.data)
        pdent.delete()
        return response


# POST direct_messages/new
class New(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    throttle_classes = (KarmaRateThrottle,)

    def post(self, request, format=None):
        if not request.user.has_perm('dent.can_use_privatedents'):
            raise rex.PermissionDenied
        serializer = DirectMessageSerializer.new(request, data=request.data)
        if serializer.is_valid():
            serializer.save(poster=request.user)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
