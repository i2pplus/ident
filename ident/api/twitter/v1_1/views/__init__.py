from rest_framework import exceptions as rex
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from ident.api.twitter.v1_1.serializers import *


class ItemNotImplemented(APIView):
    def get(self, request, **kwargs):
        raise rex.PermissionDenied

    def post(self, request, **kwargs):
        raise rex.PermissionDenied


class ListNotImplemented(APIView):
    def get(self, request, **kwargs):
        return Response([])


class CursorNotImplemented(APIView):
    def get(self, request, format=None):
        return Response(UsersSerializer({
            'previous_cursor': 0,
            'next_cursor': 0,
            self.field: []
        }).data)


class IdsCursorNotImplemented(CursorNotImplemented):
    field = 'ids'

class ListsCursorNotImplemented(CursorNotImplemented):
    def get(self, request, format=None):
        return Response(ListsSerializer({
            'previous_cursor': 0,
            'next_cursor': 0,
            'lists': []
        }).data)

class UsersCursorNotImplemented(CursorNotImplemented):
    field = 'users'


class BaseStatusList(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        # Get Dents
        dents = self.get_dents(request)
        # Range filters
        if request.GET.has_key('since_id'):
            dents = dents.filter(id__gt=request.GET['since_id'])
        if request.GET.has_key('max_id'):
            dents = dents.filter(id__lte=request.GET['max_id'])
        if request.GET.has_key('count'):
            dents = dents[:request.GET['count']]
        # Post-count filters
        dents = self.filter_dents(request, dents)
        # Convert and return
        return Response(StatusSerializer.new(request, dents, many=True).data)

    def filter_dents(self, request, dents):
        return dents
