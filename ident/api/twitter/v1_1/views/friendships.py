from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils.datastructures import MultiValueDictKeyError

from rest_framework import exceptions as rex
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from ident.api.utils import catch_and_raise
from ident.api.twitter.v1_1.serializers import *
from ident.api.twitter.v1_1.utils import get_user


# GET friends/ids
class FriendsIds(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        u = get_user(request)
        stringify = request.GET.has_key('stringify_ids') and request.GET['stringify_ids'] == 'true'
        cls = StringifiedUsersSerializer if stringify else UsersSerializer
        return Response(cls({
            'previous_cursor': 0,
            'next_cursor': 0,
            'ids': u.profile.subscriptions.values_list('id', flat=True),
        }).data)


# GET followers/ids
class FollowersIds(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        u = get_user(request)
        stringify = request.GET.has_key('stringify_ids') and request.GET['stringify_ids'] == 'true'
        cls = StringifiedUsersSerializer if stringify else UsersSerializer
        return Response(cls({
            'previous_cursor': 0,
            'next_cursor': 0,
            'ids': u.followers.values_list('user__id', flat=True),
        }).data)


# GET friends/list
class FriendsList(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        u = get_user(request)
        kwargs = {}
        if request.GET.has_key('skip_status'):
            kwargs['skip_status'] = request.GET['skip_status']
        if request.GET.has_key('include_user_entities'):
            kwargs['include_user_entities'] = request.GET['include_user_entities']
        return Response(UsersSerializer({
            'previous_cursor': 0,
            'next_cursor': 0,
            'users': u.profile.subscriptions.all(),
        }, **kwargs).data)


# GET followers/list
class FollowersList(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        u = get_user(request)
        kwargs = {}
        if request.GET.has_key('skip_status'):
            kwargs['skip_status'] = request.GET['skip_status']
        if request.GET.has_key('include_user_entities'):
            kwargs['include_user_entities'] = request.GET['include_user_entities']
        return Response(UsersSerializer({
            'previous_cursor': 0,
            'next_cursor': 0,
            'users': [p.user for p in u.followers.all()],
        }, **kwargs).data)


# POST friendships/create
class Create(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        u = get_user(request)
        request.user.profile.subscriptions.add(u)
        serializer = UserSerializer.new(request, u)
        return Response(serializer.data)


# POST friendships/destroy
class Destroy(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        u = get_user(request)
        request.user.profile.subscriptions.remove(u)
        serializer = UserSerializer.new(request, u)
        return Response(serializer.data)


# GET friendships/show
class Show(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def get(self, request, format=None):
        users = User.objects
        if request.GET.has_key('source_id'):
            source = users.get(pk=request.GET['source_id'])
        else:
            source = users.get(username=request.GET['source_screen_name'])
        if request.GET.has_key('target_id'):
            target = users.get(pk=request.GET['target_id'])
        else:
            target = users.get(username=request.GET['target_screen_name'])

        sp = source.profile
        tp = target.profile
        s_following_t = sp.subscriptions.filter(id=target.id).count() > 0
        t_following_s = tp.subscriptions.filter(id=source.id).count() > 0
        serializer = ShowFriendshipSerializer({'relationship': {
            'source': {
                'id': source.id,
                'username': source.username,
                'following': s_following_t,
                'followed_by': t_following_s,
                'can_dm': t_following_s,
            },
            'target': {
                'id': target.id,
                'username': target.username,
                'following': t_following_s,
                'followed_by': s_following_t,
            },
        }})
        return Response(serializer.data)


# GET friendships/lookup
class Lookup(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def get(self, request, format=None):
        screen_names = []
        user_ids = []
        if request.GET.has_key('screen_name'):
	    screen_names = request.GET['screen_name'].split(',')
        if request.GET.has_key('user_id'):
            user_ids = request.GET['user_id'].split(',')
        users = User.objects.filter(
            Q(username__in=screen_names) | Q(id__in=user_ids))[:100]
        return Response(RelationshipLookupSerializer(users, many=True).data)
