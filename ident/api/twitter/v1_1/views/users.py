from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils.datastructures import MultiValueDictKeyError

from rest_framework import exceptions as rex
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from ident.api.utils import catch_and_raise
from ident.api.twitter.v1_1.serializers import *
from ident.api.twitter.v1_1.utils import get_user


# GET users/lookup
class Lookup(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        return self.lookup(request)

    def post(self, request, format=None):
        return self.lookup(request)

    @catch_and_raise(MultiValueDictKeyError, rex.ParseError)
    @catch_and_raise(ObjectDoesNotExist, rex.NotFound)
    def lookup(self, request):
        screen_names = []
        user_ids = []
        if request.GET.has_key('screen_name'):
	    screen_names = request.GET['screen_name'].split(',')
        if request.GET.has_key('user_id'):
            user_ids = request.GET['user_id'].split(',')
        users = User.objects.filter(
            Q(username__in=screen_names) | Q(id__in=user_ids))[:100]
        return Response(UserSerializer.new(request, users, many=True).data)


# GET users/show
class Show(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        u = get_user(request)
        serializer = UserSerializer.new(request, u)
        return Response(serializer.data)
