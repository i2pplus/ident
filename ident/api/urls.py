from django.conf.urls import include, url

#from ident.api.handlers import *
from ident.dent.views import JsonLastDentDetailView, APIDentCreateView
from ident.user.views import JsonUserView, list_users_as_json

#auth = OAuthAuthentication(realm='http://id3nt.i2p')
#root_handler = Resource(RootHandler)
#dents_handler = Resource(DentsHandler, authentication=auth)
#dent_handler = Resource(DentHandler)
#users_handler = Resource(UsersHandler)
#user_handler = Resource(UserHandler)

urlpatterns = [
    # API v1
    url(r'^1/u/id_(?P<pk>\d+)/$', JsonUserView.as_view()),
    url(r'^1/u/(?P<username>[\w-]+)/$', JsonUserView.as_view()),
    url(r'^1/u/$', list_users_as_json), #user list
    url(r'^1/u/(?P<username>[\w-]+)/d/last/$', JsonLastDentDetailView.as_view()),
    url(r'^1/d/create/$', APIDentCreateView.as_view()),

    # API v2.0
#    url(r'^2.0/u/id_(?P<pk>\d+)/', user_handler),
#    url(r'^2.0/u/(?P<username>[\w-]+)/', user_handler),
#    url(r'^2.0/u/', users_handler),
#    url(r'^2.0/d/(?P<pk>\d+)/', dent_handler),
#    url(r'^2.0/d/', dents_handler),
#    url(r'^2.0/', root_handler),

    # Twitter-like APIs
    url(r'^twitter/', include('ident.api.twitter.urls')),
]

# OAuth
#urlpatterns += [
#    url(r'^oauth/request_token/$', oauth_request_token, name='oauth_request_token'),
#    url(r'^oauth/authorize/$', oauth_user_auth, name='oauth_user_auth'),
#    url(r'^oauth/access_token/$', oauth_access_token, name='oauth_access_token'),
#]
