import datetime
from rest_framework import throttling

from ident.dent.models import Dent

class catch_and_return(object):
    def __init__(self, err, response):
        self.err = err
        self.response = response

    def __call__(self, fn):
        def wrapper(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except self.err:
                return self.response
        return wrapper

class catch_and_raise(object):
    def __init__(self, err, response):
        self.err = err
        self.response = response

    def __call__(self, fn):
        def wrapper(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except self.err:
                raise self.response
        return wrapper

class KarmaRateThrottle(throttling.BaseThrottle):
    def allow_request(self, request, view):
        if request.user.has_perm('user.new_user_restrictions_lifted'):
            # Allow 5 dents every 10 minutes
            sincetime = datetime.datetime.now() - datetime.timedelta(minutes=10)
            dent_count = Dent.objects.filter(poster=request.user, time__gt=sincetime).count()
            if dent_count < 5:
                return True
        else:
            # Allow 2 dents every 24 hours
            sincetime = datetime.datetime.now() - datetime.timedelta(days=1)
            dent_count = Dent.objects.filter(poster=request.user, time__gt=sincetime).count()
            if dent_count < 2:
                return True
        return False
