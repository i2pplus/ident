from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from oauth_provider.models import Token
from oauth_provider.views import AuthorizeRequestTokenForm

@login_required
def oauth_authorize(request, request_token, callback_url, params):
    c = RequestContext(request)
    if request.method != 'POST':
        form = AuthorizeRequestTokenForm()
        return render_to_response('oauth/authorize.html',
                                  {'request_token': request_token},
                                  context_instance=c)

@login_required
def oauth_verified(request, oauth_token):
    token = Token.objects.get(key=oauth_token)
    c = RequestContext(request)
    return render_to_response('oauth/verified.html',
                              {'verifier': token.verifier},
                              context_instance=c)
