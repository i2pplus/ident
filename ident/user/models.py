import os.path
import datetime
from math import log

from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import Permission, User
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.db import models
from django.db.models.signals import m2m_changed, post_save
from django.db.models import Max, Count
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _

from gadjo.requestprovider.signals import get_request
from messages_extends import constants as constants_messages
from reputation.models import Reputation
from stdimage import StdImageField

from ident.dent.fields import LanguageField
from ident.dent.models import Dent, Vote
from ident.views import MessageView

class IdentStdImageField (StdImageField):
    """Extends the StdImageField with sanity checks for file extensions.

    It is used to store a users Avatar image."""
    def validate(self, value, model_inst):
        """Check for valid file extensions and pass on to parent clean()

        Parent already checks that the content is a valid image, but it doesn't check for file extension. But StdImageField will crap out lacking a file extension when it wants to resize. So, this makes sure we have the file extension we need."""
        super(IdentStdImageField, self).validate(value, model_inst)
        # Only do something if a new file has been uploaded.
        basename, extension = os.path.splitext(str(value))
        if not extension.lower() in ['.png','.jpg','.jpeg']:
            raise forms.ValidationError(_("Only jpg and png images are supported. "
                                    "Please use .png or .jpg as file extension."))


class Profile(models.Model):
    user = models.OneToOneField(User)
    email_is_public = models.BooleanField(default=False, verbose_name="E-mail",
              help_text= _('Display e-mail publicly.'))
    i2pbote = models.CharField(max_length=3000, blank=True,
              help_text=_('Your <a href="http://i2pbote.i2p/">i2pbote</a> '
                          'address (publicly shown).'))
    i2pmessenger = models.CharField(max_length=512, blank=True,
              verbose_name="I2Pmsn",
              help_text=_('Your <a href="http://echelon.i2p/qti2pmessenger/">'
                          'i2pmessenger</a> address (publicly shown).'))
    nightweb = models.CharField(max_length=512, blank=True,
              help_text=_('Your <a href="https://nightweb.net/">'
                          'Nightweb</a> userhash (publicly shown).'))
    description = models.CharField(max_length=1024, blank=True,
                  help_text=_('A brief, public, description of yourself.'))
    homepage = models.CharField(max_length=250, blank=True, verbose_name='WWW',
               help_text=_('Your Website (publicly shown).'))
    avatar   = IdentStdImageField(upload_to='avatars', blank=True,
               verbose_name='avatard',
               size=(150, 200), thumbnail_size=(40, 40),
               help_text=_('Image in png or jpg format, will be scaled down to '
                           'max. 150x200 pixels. Square images preferred for '
                           'nice small thumbnails. Leave this empty to keep the '
                           'current unchanged.'))
    subscriptions = models.ManyToManyField(User, blank=True,
                                           related_name='followers')
    karma = models.SmallIntegerField(editable=False, blank=True, default=0,
                                help_text='Your karma')
    """karma is dynamically updated represents a users karma"""


    def __unicode__(self):
        return self.user.username

    @models.permalink
    def get_absolute_url(self):
        #return self.user.get_absolute_url() + "profile/"
        return ('profilepage', [self.user.username])

    def get_avatar_thumb_url(self):
        """Returns (is_thumb, IMG_URL)

        For some users, we need to grab the thumbnail url, and some
        users can use their proper avatar image. Return the URL to the
        correct image."""
        if not self.avatar:
            # Use default avatar.
            return ("%savatars/_default.png" % settings.MEDIA_URL)
        elif not os.path.isfile(self.avatar.thumbnail.path()):
            return (self.avatar.url)
        else:
            return self.avatar.thumbnail.url()


    def dent_count(self):
        count = Dent.objects.filter(poster=self.user).count()
        return count

    def last_dent(self):
        latest = Dent.objects.filter(poster=self.user)[:1]
        return None if latest.count() == 0 else latest[0]

    def last_dent_time(self):
        """returns datetime object or none"""
        timestamp = Dent.objects.filter(poster=self.user).aggregate(Max('time'))['time__max']
        return timestamp

    def subscription_list(self):
        """list of users we subscribed to"""
        return list(self.subscriptions.all())

    def check_new_user_restrictions(self):
        """Checks if new user restrictions should be lifted"""
        permission = Permission.objects.get(codename='new_user_restrictions_lifted')
        dent_count = self.user.dent_set.count()
        up_vote_count = Vote.objects.filter(dent__poster=self.user, value=1).count()
        down_vote_count = Vote.objects.filter(dent__poster=self.user, value=-1).count()
        if dent_count >= 5 or \
          (dent_count > 0 and up_vote_count > down_vote_count) or \
          (dent_count > 0 and up_vote_count >= 2):
            try:
                self.user.user_permissions.get(id=permission.id)
            except Permission.DoesNotExist:
                self.user.user_permissions.add(permission)
        else:
            try:
                self.user.user_permissions.get(id=permission.id)
                self.user.user_permissions.remove(permission)
            except Permission.DoesNotExist:
                pass

    class Meta:
        ordering=['user']
        permissions = (('new_user_restrictions_lifted', _('New user restrictions lifted')),)

#Automatically create Profiles when creating users:
def profile_creator(sender, instance, created, **kwargs):
    """Create a Profile,SiteSettings,Reputation everytime a new User is created.

    This is a callback function listening to the signal."""
    if created:
        # create Profile and SiteSettings too
        profile, new = Profile.objects.get_or_create(user=instance)
        profile, new = SiteSettings.objects.get_or_create(user=instance)
        Reputation.objects.reputation_for_user(instance)
post_save.connect(profile_creator, sender=User)

# signal that adds a 'message' when the user logged in
def display_logged_in_msg (sender, request, user, **kwargs):
    messages.info(request, _("Welcome back, '%(user)s'! We missed you...") % {'user': user})
    #Until all users have one, make sure users have a SiteSettings object.
    setting, create = SiteSettings.objects.get_or_create(user=user)
    #set the session timeout per user setting
    if setting.session_timeout is not None:
        request.session.set_expiry(setting.session_timeout*60)
user_logged_in.connect(display_logged_in_msg, weak=False,
                       dispatch_uid="unique_login")

# signal that adds a 'message' when the user logged in
def display_logged_out_msg (sender, request, user, **kwargs):
    if user:
        messages.info(request, _("Buh bye '%(user)s'! See you soon we hope!") % {'user': user})
user_logged_out.connect(display_logged_out_msg, weak=False,
                        dispatch_uid="unique_login")

def update_karma(sender, instance, created, **kwargs):
    """Update the user's karma based on their Reputation.

    This is a callback function listening to the signal."""
    profile = instance.user.profile
    profile.karma = instance.reputation
    profile.save()
post_save.connect(update_karma, sender=Reputation)

def notify_permissions(sender, instance, **kwargs):
    action = kwargs['action']
    if action in ['post_add', 'post_remove']:
        reverse = kwargs['reverse']
        if not reverse:
            model = kwargs['model']
            pk_set = kwargs['pk_set']
            permissions = model.objects.filter(pk__in=pk_set)
            # Only send notifications if we can get a request.
            try:
                request = get_request()
                if request:
                    for permission in permissions:
                        perm_add_msg = _('Congratz! Your karma has increased enough to give you access to "%(permission_name)s"') % {'permission_name': permission.name}
                        perm_remove_msg = _('Oh dear! Your karma has dropped and you have lost access to "%(permission_name)s"') % {'permission_name': permission.name}
                        if permission.codename == 'new_user_restrictions_lifted':
                            perm_add_msg = _('Congratz! New user restrictions have been lifted, and you are now a full id3ntist!')
                            perm_remove_msg = _('Oh dear! You have reverted to new user status. Post some dents.')
                        elif permission.codename == 'can_vote_up':
                            perm_add_msg = _("Congratulations, you've been awared the gift of upvoting dents. Enjoy responsibly!")
                        elif permission.codename == 'can_vote_down':
                            perm_add_msg = _('Down votes activated! Abuse to lose!')
                        elif permission.codename == 'can_use_privatedents':
                            perm_add_msg = _('Celebrate, rejoice! StalkMail now enabled!')
                        if action == 'post_add':
                            messages.add_message(
                                request,
                                constants_messages.SUCCESS_PERSISTENT,
                                perm_add_msg,
                                user=instance)
                        else:
                            messages.add_message(
                                request,
                                constants_messages.WARNING_PERSISTENT,
                                perm_remove_msg,
                                user=instance)
            except:
                pass
m2m_changed.connect(notify_permissions, sender=User.user_permissions.through, weak=False)

#-------------End of Profile -- Start of SiteSettings ----------------

class SiteSettings(models.Model):
    """SiteSettings contain settings which a user uses to personalize
    their account but which do not necessarily need to be pulled as
    part of the profile just to display someones dents."""
    user = models.OneToOneField(User)
    """Link back to the main user"""

    language = LanguageField(blank=True, default='en', help_text=_(
                      "The language to use when displaying the site. Your "
                      "dents will also be tagged as this language when their "
                      "content language cannot be guessed."))

    class IndexPages:
        HOMEPAGE   = "homepage"
        DASHBOARD  = "dashboard"
        SUBSCRIBED = "subscribed"
        USERPAGE   = "userpage"
    INDEX_PAGE_CHOICES = (
        (IndexPages.HOMEPAGE,  _("Freshest Dents")),
        #(IndexPages.DASHBOARD, _("Your Dashboard")),
        (IndexPages.SUBSCRIBED, _("Your Stalk-o-vision")),
        (IndexPages.USERPAGE,  _("Your User Page")),
    )
    index_page = models.CharField(max_length=12,
                                  choices=INDEX_PAGE_CHOICES,
                                  default=IndexPages.HOMEPAGE,
                                  help_text=_(
                      "The page to use as the site index while logged in."))

    session_timeout = models.PositiveSmallIntegerField(null=True, blank=True,
                                                       default=None,
                                                       help_text=_(
                      "Max site inactivity (mins) before auto-logout "
                      "(0 for never)."))

    class Themes:
        BURGUNDY = "burgundy"
        CORPORAT = "corporat"
    THEME_CHOICES = (
        (Themes.BURGUNDY, "Burgundy"),
        (Themes.CORPORAT, "Corporat"),
    )
    theme = models.CharField(max_length=12,
                             choices=THEME_CHOICES,
                             default=Themes.BURGUNDY,
                             help_text=
               "The site theme to use.")

    user_css = models.TextField(default="", blank=True, help_text=_(
               "Additional CSS which "
               "will be applied to all pages in addition to the site CSS, use "
               "this to customize ID3NT to your liking."))

    def __unicode__(self):
        return "%ss' site settings" % self.user.username
