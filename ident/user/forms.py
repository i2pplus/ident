from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm, ValidationError
from django.utils.translation import ugettext as _

from ident.user.models import Profile, SiteSettings

DISALLOWED_USERNAMES = [
    '.',
    '..',
]

# Create the form class.
class UserEmailForm(ModelForm):
    class Meta:
        model = User
        fields = ('email',)

# Create the form class.
class UserProfileForm(ModelForm):
    class Meta:
        model = Profile
        exclude = ('user','subscriptions')

    def clean_nightweb(self):
        nightweb = self.cleaned_data['nightweb']
        prefix = 'http://nightweb.net/#type=user&userhash='
        # If the user submits their Nightweb url,
        # extract their userhash
        if prefix in nightweb:
            nightweb = nightweb.split(prefix)[1]
        return nightweb

class UserSiteSettingForm(ModelForm):
    class Meta:
        model = SiteSettings
        exclude = ('user',)

class IdentUserCreationForm(UserCreationForm):
    def clean_username(self):
        username = self.cleaned_data['username']
        if username in DISALLOWED_USERNAMES:
            raise ValidationError(_('Username Selection Fail: This username is not allowed.'))
        if username.startswith('@'):
            raise ValidationError(_('Your username cannot begin with an @ symbol.'))
        return self.cleaned_data['username']

class UserCreationSiteSettingForm(ModelForm):
    class Meta:
        model = SiteSettings
        fields = ('language',)
