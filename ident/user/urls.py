from django.conf.urls import url
from django.views.generic import RedirectView

from ident.dent.feed import LatestUserEntriesFeed,LatestUserStalkingFeed
from ident.dent.views import UserDentListView
from ident.user import views

# user RSS feed
urlpatterns = [
    url(r'^((?P<username>[^/]+))/feed$', LatestUserEntriesFeed(),
        name='latest_user_feed'),
    url(r'^((?P<username>[^/]+))/stalking/feed$', LatestUserStalkingFeed(),
        name='latest_stalking_feed')
]
# handle URL: /u/...
urlpatterns += [
    url(r'^/?$', views.list_users), #List of all users
    url(r'^(?P<username>.*[^/])/profile/?$', views.UserProfileView.as_view(),
        name='profilepage'),
    url(r'^(?P<username>.*[^/])/dashboard/?$', views.UserDetailView.as_view(),
        name='user_dashboard'),
    # user profile change user email is now integrated in regular
    # profile edit, so redirect there
    url(r'^(?P<username>.*[^/])/profile/change_email/?$', RedirectView.as_view(
            url='/u/%(username)s/profile/edit/')),
    url(r'^(?P<username>.*[^/])/profile/edit/?$', views.edit_profile,
        name='edit_profile'), #edit user profile
    url(r'^(?P<username>.*[^/])/delete/?$', views.user_delete,
        name='user_delete'), #delete user!
    url(r'^(?P<username>.*[^/])/stalk/?$', views.user_follow,
        name='user_follow'), #follow user
    url(r'^(?P<username>.*[^/])/unstalk/?$', views.user_unfollow,
        name='user_unfollow'), #follow user
    url(r'^(?P<username>[^/]+)/stalking(/by_(?P<sort_by>[-\w]+))?/?$',
        UserDentListView.as_view(), {'stalkovision': True},
        name='stalk-o-vision'), #stalkovision view
    url(r'^(?P<username>[^/]+)(/by_(?P<sort_by>[-\w]+))?/?$',
        UserDentListView.as_view(),
        name='user_home'), #display user homepage
]
