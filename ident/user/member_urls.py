from django.conf.urls import url

from ident.user.views import UserListView

# handle URL: /members/...
urlpatterns = [
    #List of all users sorted by...
    url(r'^by_username/?$', UserListView.as_view(),
        {'sort_by':'username'},
        name='membership_by_username'),
    url(r'^by_-username/?$', UserListView.as_view(),
        {'sort_by':'-username'},
        name='membership_by_-username'),
    url(r'^by_dents/?$', UserListView.as_view(),
        {'sort_by':'dents'},
        name='membership_by_dents'),
    url(r'^by_-dents/?$', UserListView.as_view(),
        {'sort_by':'-dents'},
        name='membership_by_-dents'),
    url(r'^by_karma/?$', UserListView.as_view(),
        {'sort_by':'-karma'},
        name='membership_by_karma'),
    url(r'^by_-karma/?$', UserListView.as_view(),
        {'sort_by':'karma'},
        name='membership_by_-karma'),
    url(r'^(by_(?P<sort_by>[-\w]+))?/?$', UserListView.as_view(),
        name='membership_page'),
    #url(r'^(/by_(?P<sort_by>[-\w]+))?/?$', UserListView.as_view(),
    #    name='membership_by_username'),
]
