import os
import datetime

from django.template import RequestContext
from django.core.files.base import ContentFile
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.db.models import Max, Sum, Count, F
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.utils.translation import activate as t_act, deactivate as t_deact, \
    get_language as t_lang, ugettext as _
from django.views.generic import DetailView, RedirectView, ListView, DeleteView
try:
    import simplejson as json
except ImportError:
    import json

from ident.dent.models import Dent
from ident.user.forms import UserEmailForm, UserProfileForm, UserSiteSettingForm, \
    IdentUserCreationForm, UserCreationSiteSettingForm
from ident.views import IdentViewMixin


class UserDetailView(IdentViewMixin, DetailView):
    """Returns the User dashboard view"""
    model=User
    template_name="user/dashboard.html"
    context_object_name="u"

    def get_object(self):
        if self.kwargs.get('username', None):
            return get_object_or_404(User, username=self.kwargs['username'])
        return super(UserDetailView, self).get_object()


class JsonUserView(DetailView):
    """Returns the Usr details as a json object"""
    #context_object_name = "publisher"
    model=User

    def get_object(self):
        if self.kwargs.get('username', None):
            return get_object_or_404(User, username=self.kwargs['username'])
        return super(JsonUserView, self).get_object()

    def get_userinfo_as_json(self):
        u = self.get_object()
        p = u.profile
        obj= {}
        obj['username'] = u.username
        obj['homepage'] = p.homepage
        obj['I2PBote'] = p.i2pbote
        obj['I2PMessenger'] = p.i2pmessenger
        obj['Nightweb'] = p.nightweb
        if p.email_is_public:
            obj['email'] = u.email
        obj['num_dents'] = p.dent_count()
        obj['last_dents'] = str(p.last_dent_time())
        obj['joined'] = str(u.date_joined.date())
        obj['karma'] = p.karma
        obj['stalking'] = list(p.subscriptions.all().values_list('username', flat=True))
        obj['stalkers'] = list(u.followers.all().values_list('user__username', flat=True))
        jstr = json.dumps(obj)
        return jstr

    def get(self, request, *args, **kwargs):
        return HttpResponse(self.get_userinfo_as_json())

class UserProfileView(DetailView):
    """Display a user's profile page

    Requires that a user be logged in and takes keyword argument
    username, if no username is given, we use the currently logged in
    user."""
    model = User
    template_name = "user/profile.html"
    context_object_name="u"

    def get_object(self):
        """Return the User object

        NoOp, here, we set the self.object alread during dispatch"""
        return self.object

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """override dispatch with a csrf_excempt decorator"""
        #check authentication. TODO factor out:
        #raise Exception(str(dir(request.user)))
        self.object = request.user
        if kwargs.has_key('username') and \
                self.object.username != kwargs['username']:
            messages.warning(request, _("You can only view your own profile "
                             "pages!"))
            profile_page = self.object.profile.get_absolute_url()
            return redirect(profile_page)
        return super(UserProfileView, self).dispatch(
            request, *args, **kwargs)


class UserListView(IdentViewMixin, ListView):
    """Display the 'membership' page"""
    model = User
    context_object_name = "user_list"
    paginate_by=50
    template_name='user/user_list.html'

    def sort_queryset_by(self, queryset):
        sort_by = self.kwargs.get('sort_by', None)
        if sort_by is None:
            #default: by last activity
            queryset = queryset.annotate(Max('dent__time')).order_by('dent__time__max').reverse()
        elif sort_by in ['karma', '-karma']:
            queryset = queryset.order_by('profile__karma')
        elif sort_by in ['dents', '-dents']:
            queryset = queryset.annotate(Count('dent')).order_by('dent__count')
        elif sort_by in ['username', '-username']:
            queryset = queryset.order_by('username')
        if sort_by and sort_by[0] == '-':
            queryset = queryset.reverse()
        return queryset

    def get_queryset(self):
        # Call the base implementation
        queryset= super(UserListView, self).get_queryset()
        queryset = queryset.filter(is_active=True)
        queryset = self.sort_queryset_by(queryset)
        return queryset

def list_users(request, sort_by=None, as_json=False):
    """List all active users"""
    if not as_json:
        users = User.objects.annotate(Max('dent__time')).order_by('dent__time__max').reverse()
        #allows to use date: {{user.dent__time__max}}
        return ListView.as_view(
            model=User,
            template_name='user/user_list.html'
        )(request, queryset=users, paginate_by=100)
    else:
        jstr=json.dumps(list(User.objects.all().values_list('username', flat=True)))
        return HttpResponse(jstr)

def list_users_as_json(request):
    return list_users(request, as_json=True)

@login_required
def current_user_home(request):
    # Redirect to the index, to show whatever the user has set as their index page
    return redirect('index')

@login_required
def change_email(request, username=None):
    """Change user's email or name

    This function is not really used anymore, as email changing is
    part of the regular profile changing. At some point we will pull
    this function out and remove."""
    c = RequestContext(request)
    u = request.user
    if u.username != username:
        messages.warning(request, _("You can only view your own profile pages!"))
        return redirect(u.profile)
    if request.method != "POST":
        #Display the change form
        form = UserEmailForm(instance=u)
    else:
        #for has been submitted via POST
        form = UserEmailForm(request.POST, request.FILES, instance=u)
        if form.is_valid():
            u.email = form.cleaned_data['email']
            u.first_name = form.cleaned_data['first_name']
            u.last_name = form.cleaned_data['last_name']
            u.save()
            messages.info(request, _("Profile updated. Well done!"))
            return redirect(u.profile)

        #in case of error, display those.
        messages.warning(request, _("Tard alert! Form haz eraz, please fix."))
    return render_to_response('user/change_email.html',
                                  {'u': u, 'form': form},
                                  context_instance=c)
    u = get_object_or_404(User, username = username)
    f = User.objects.filter(followers__user=u)
    d = Dent.objects.filter(poster__in=f)
    #u.profile.subscribers
    #dents = Dent.objects.filter(poster = u)
    return ListView.as_view(
        model=Dent,
        template_name='user/user.html'
    )(request, queryset=d, paginate_by=30, extra_context={'u': u, 'friends': True})

@login_required
def edit_profile(request, username):
    """Show the current users profile page"""
    c = RequestContext(request)
    u = request.user
    p = u.profile

    if u.username != username:
        messages.warning(request, _("No. Not yours. Go away!"))
        return redirect(edit_profile, username=u.username)

    if request.method != "POST":
        #Display the change form
        form_email = UserEmailForm(instance=u)
        form_profile = UserProfileForm(instance=p)
        form_setting = UserSiteSettingForm(instance=u.sitesettings)
        return render_to_response('user/edit_profile.html',
                                  {'u': u, 'form': form_profile,
                                   'form_site_setting': form_setting,
                                   'form_email': form_email},
                                  context_instance=c)
    # POST form was submitted
    # save old avatar path if one exists, so we can delete it
    try:
	old_avatar_path = p.avatar.path
    except ValueError as e:
        old_avatar_path = None

    f_profile = UserProfileForm(request.POST, request.FILES, instance=p)
    f_setting = UserSiteSettingForm(request.POST, instance= u.sitesettings)
    f_email = UserEmailForm(request.POST, instance= u)

    if f_profile.is_valid() and f_setting.is_valid() and f_email.is_valid():
        #TODO: Put the whole image saving into a helper function
        if 'avatar' in request.FILES:
            # First, delete original avatar file (if existing), then save new one.
            if old_avatar_path:
                try:
                    os.unlink(old_avatar_path)
                except OSError as e:
                    # Previous avatar file not found
                    if e.errno == 2:
                        pass
            filename = u.username
            if request.FILES['avatar'].content_type == 'image/jpeg':
                filename += '.jpg'
            elif request.FILES['avatar'].content_type == 'image/png':
                filename += '.png'
            #return HttpResponse(filename)
            #TODO: sanity checks about file size and dimensions
            file_content = ContentFile(request.FILES['avatar'].read())
            p.avatar.save(filename, file_content)

        # apply potentially changed session timeout value
        if f_setting.cleaned_data['session_timeout'] is None:
            request.session.set_expiry(None)
        else:
            request.session.set_expiry(
                f_setting.cleaned_data['session_timeout']*60)
        # save rest of the user profile changes
        f_profile.user = u
        f_profile.save()
        f_setting.user = u
        f_setting.save()
        f_email.save()
        messages.info(request,
                      _('Profile updated. Well done!'))
        return redirect(p)

    # Form was not valid, display it again, with error messages.
    #form_profile = UserProfileForm(instance=p)
    #form_setting = UserSiteSettingForm(instance=u.sitesettings)
    messages.warning(request,
                      _('Tard alert! We haz detected eraz in ur form, please to fix.'))

    return render_to_response('user/edit_profile.html',
                              {'u': u, 'form': f_profile,
                               'form_site_setting': f_setting,
                               'form_email': f_email},
                              context_instance=c)

def csrf_failure(request, reason):
    c = RequestContext(request)
    title = 'Possible Cross-site request forgery detected'
    reason = "<b>Reason: %s</b><p>This happens for example if you do not allow cookies from the domain " \
             "that you are currently visiting.<br/>Please make sure you allow cookies from either" \
             " id3nt.i2p or b32.i2p (depending on how you see this site).</p>" % reason
    return render_to_response('generic_message.html', {'title':title,'message':reason},
                              context_instance=c)

def user_register(request):
    """Register a new user name"""
    c = RequestContext(request)
    if request.method == 'POST':
        form = IdentUserCreationForm(request.POST)
        f_lang = UserCreationSiteSettingForm(request.POST)
        #ratelimit exceeded, allow 2 user every hour
        sincetime = datetime.datetime.now() - datetime.timedelta(hours=1)
        newuser_count = User.objects.filter(date_joined__gt=sincetime).count()
        if newuser_count > 3:
            messages.warning(request, _("Sorry. The ID3NT new application processing team are currently"
                " out to lunch, please come back in an hour when we'll be back in the office!"))
        elif form.is_valid():
            new_user = form.save()
            #now log in the user...
            user = auth.authenticate(username= new_user.username,
                                      password= form.cleaned_data['password1'])
            auth.login(request, user)
            f_lang = UserCreationSiteSettingForm(request.POST, instance=user.sitesettings)
            if f_lang.is_valid():
                f_lang.save()
                language = f_lang.cleaned_data['language']
                t_deact()
                t_act(language)
                request.LANGUAGE_CODE = t_lang()
            messages.info(request, _("Welcome to ID3NT, '%(user)s', mind your step as you board the vessel,"
                " hop over to <a href=\"/about\">the about page</a> to get a feel for how things operate, and enjoy the next"
                " generation of mass brainwashing apparatus previously only available to state-level"
                " operatives!") % {'user': user})
            messages.info(request, _("Registering an e-mail account for password recovery & optional display"
                " on your profile is <i>severely</i> recommended."))
            return redirect(new_user.profile)
    else:
        form = IdentUserCreationForm()
        f_lang = UserCreationSiteSettingForm({'language': request.LANGUAGE_CODE})
    return render_to_response("registration/registration_form.html", {
        'form' : form,
        'f_lang': f_lang,
    }, context_instance=c)

@login_required
def user_delete(request, username):
    """Delete a user and all related Dents (if we have permission)

    If we add a name="realdelete" value=1 to the form, we delete the
    user object and all related dents/votes from the db. Otherwise, we
    effectively only set the is_active attribute to False."""
    u = get_object_or_404(User, username = username)

    # object/url we want to redirect after this
    next = request.GET.get('next')
    if not next:
        next = request.user.get_absolute_url()

    has_permission = (u == request.user) or \
        request.user.has_perm('auth.delete_user')

    if not has_permission:
        messages.warning(request, _("Not possible. Permission fail on delete: '%(user)s'") % {'user': username})
        return redirect(next)

    # either display confirmation form, or REALLY delete a user
    if request.method == 'GET' or request.POST.get('realdelete', False):
        return DeleteView.as_view(
            model=User,
            success_url=next,
            template_name='user/user_confirm_delete.html'
        )(request, pk=u.id)
    else:
        messages.warning(request, _("User '%(user)s' has been neutered.") % {'user': u})
        u.is_active=False
        u.save()
        return redirect(next)

@login_required
def user_follow(request, username=None, dent_id=None, unfollow=False):
    """Let authorized user subscribe to user 'user'

    Can be invoked with either a username or a dent id"""
    c = RequestContext(request)
    u = request.user
    p = u.profile
    if username:
        follow = get_object_or_404(User, username = username)
    elif dent_id:
        follow = get_object_or_404(Dent, id = dent_id).poster
    else:
        return HttpResponse(_("Incomprehensible! Called without dent or user id"))

    if unfollow:
        # Remove subscription
        p.subscriptions.remove(follow)
        reason = _("TADA! Not stalking '%(user)s' anymore!") % {'user': follow}
    elif u == follow:
        reason = _("You can't stalk yourself, idiot!")
    else:
        # Add the subscription
        p.subscriptions.add(follow)
        reason = _("You are now stalking '%(user)s'") % {'user': follow}

    messages.info(request, reason)

    # optional url we want to redirect after this
    next = request.GET.get('next')
    if next:
        return redirect(next)

    title = _("Subscribe to user")
    return render_to_response('generic_message.html',
                              {'title':title,'message':reason},
                              context_instance=c)

def user_unfollow(request, username=None, dent_id=None):
    return user_follow(request, username, dent_id, unfollow=True)
