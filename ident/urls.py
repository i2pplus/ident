from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from reputation.registry import reputation_registry

from ident.dent.feed import LatestEntriesFeed
from ident.dent.models import DentingReputationHandler, VotingReputationHandler
from ident.dent.views import DentListView, FeedbackPrivateDentCreateView
from ident.user.views import UserProfileView, current_user_home, user_register
from ident.views import IdentTemplateView, StatisticsView, MessageView, index_handler

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

reputation_registry.register(DentingReputationHandler)
reputation_registry.register(VotingReputationHandler)

urlpatterns = [
    # Homepage view of recent dents:
    url(r'^/?$', index_handler,
        name="index"),
    url(r'^all(/by_(?P<sort_by>[-\w]+))?\/?$', DentListView.as_view(),
        name="homepage"),
    url(r'^u/', include('ident.user.urls')),
    url(r'^members/', include('ident.user.member_urls')),
    url(r'^api/', include('ident.api.urls')),
    url(r'^accounts/logged_in/', current_user_home),
    url(r'^accounts/password\/?$', UserProfileView.as_view()),
    # Don't use registration backend, we don't want email confirmation.
    #url(r'^accounts/', include('registration.backends.default.urls')),
    # For all the rest use the defaults
    url(r'^accounts/register/$', user_register),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^search/', include('ident.search.urls')),
    url(r'^hashtags/', include('hashtags.urls')),
    url(r'^dent/', include('ident.dent.urls')),
    url(r'^private/', include('ident.dent.private_urls')),
    url(r'^feed$', LatestEntriesFeed()),
    url(r'^about/?$', IdentTemplateView.as_view(template_name='about.html'),
        name='about_page'),
    url(r'^usage/?$', IdentTemplateView.as_view(template_name='usage.html'),
        name='api_usage'),
    url(r'^donate/?$', IdentTemplateView.as_view(template_name='donate.html'),
        name='donate_page'),
    url(r'^stats/?$', StatisticsView.as_view(),
        name='statistics_main'),
    url(r'^hello-spider/?$', DentListView.as_view(template_name= 'geti2p.html'),
                   {'sort_by':'vote'}),
    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    #url(r'^admin/(.*)', admin.site.root),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^msg$', MessageView.as_view(), name='generic_message'),
    url(r'^messages/', include('messages_extends.urls')),
    url(r'^feedback/?$', login_required(FeedbackPrivateDentCreateView.as_view()),
        name='feedback_page'),
]

if settings.DEBUG:
    urlpatterns += [
        # Make sure you serve the /media files through your web server
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': '/home/str4d/dev/git/ident/ident/media/'}),
    ]
