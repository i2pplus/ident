from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template import RequestContext

from ident.dent.models import Dent
#from ident.search.forms import SearchForm

def submit_search(request, searchpath):
    """Create and save a new dent"""
    if request.method != 'POST' or not request.POST.has_key('searchterm'): # No form has been submitted
        c = RequestContext(request)
        return render_to_response('search.html', context_instance=c)

    searchterm = request.POST.get('searchterm')
    return redirect(searchpath+searchterm)
