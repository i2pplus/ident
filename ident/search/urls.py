from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from ident.dent.views import DentListView
from ident.search.views import submit_search

urlpatterns = [
    url('^\/?$', submit_search, {'searchpath': '/search/'}, "submit_search"),
    url(r'^(?P<searchterm>.+)\/?$', login_required(DentListView.as_view()), name="search_list"),
]
