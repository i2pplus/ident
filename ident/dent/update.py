import datetime
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db.models import Sum
from django.template.defaultfilters import force_escape, striptags, urlizetrunc,\
    linebreaks

from guess_language import guess_language
from reputation.models import Reputation, ReputationAction

from ident.dent.models import Dent, DentingReputationHandler,\
                              Vote, VotingReputationHandler
from ident.user.models import User

def clean_text(dent):
    text = dent.text
    # escape HTML tags instead of stripping them
    text = force_escape(text)
    #text = striptags(self.text)
    text = urlizetrunc(text, 60)
    #disabled by dr|z3d: text = linebreaks(text)

    #find overly long words and truncate them
    text = Dent.truncate_re.sub('\\1<span class="truncated" title="\\g<longword>">[\\3&hellip;]</span>', text)

    # find @User and convert them to links
    match = Dent.atUser_re.match(text)
    while match:
        username = match.group('username')
        try:
            User.objects.get(username=username)
            # User exists, so do a straight substitution
            text = Dent.atUser_re.sub('\\1<a href="/u/\\g<username>">@\\g<username></a> ', text, 1)
        except User.DoesNotExist:
            try:
                matchedUser = User.objects.get(username__startswith=username)
                # A single User beginning with the supplied name exists, so use that user instead
                text = Dent.atUser_re.sub('\\1<a href="/u/%(username)s">@%(username)s</a> ' % {'username': matchedUser.username}, text, 1)
            except User.DoesNotExist:
                print "The user %s in dent %d does not exist." % (username, dent.id)
                break
            except User.MultipleObjectsReturned:
                print "@%s in dent %d matches several users." % (username, dent.id)
                break
        match = Dent.atUser_re.search(text)

    # find hashtags and convert them to links
    text = Dent.hashtag_re.sub('\\1<a href="/search/tag/\\g<hashtag>">#\\g<hashtag></a>', text)
    dent.text = text

def update_content(dent):
    dent.popularity = dent.calculate_popularity()
    dent.damped_popularity = dent.calculate_damped_popularity()
    dent.save()

def populate_vote_time():
    # Historic vote times are not known, so they need to be faked.
    #
    # Limits can be placed on possible times:
    # - after the dent was created
    # - after the voter was created
    # - before the voter last logged in + 1 day
    #   (to allow for votes placed the last time the voter logged in)
    #
    # The order that votes were placed in is known from their id.
    # The fake time for each vote is chosen as follows:
    # - A spacing is chosen between the earliest possible time for
    #   the first dent and the latest possible time for the last dent.
    # - The fake time is set to the earliest possible time + the
    #   interval * the vote id
    one_day = datetime.timedelta(days=1)
    one_hour = datetime.timedelta(hours=1)
    num_votes = Vote.objects.count()
    first_vote = Vote.objects.get(id=1)
    earliest_possible_time = max([first_vote.dent.time, first_vote.voter.date_joined])
    last_vote = Vote.objects.get(id=num_votes)
    latest_possible_time = last_vote.voter.last_login + one_day
    interval = (latest_possible_time - earliest_possible_time)/(num_votes+1)

    last_allocated_time = earliest_possible_time
    for v in Vote.objects.order_by('id'):
        v.time = earliest_possible_time + interval*v.id
        earliest_time = max([v.dent.time, v.voter.date_joined])
        latest_time = v.voter.last_login + one_day
        # Force the time to be later than the earliest time
        if v.time <= earliest_time:
            v.time = earliest_time + one_hour
        # Force the time to be earlier than the latest time
        if v.time >= latest_time:
            v.time = latest_time - one_hour
        # If the time is now within the day before the voter last logged
        # in, assume the vote was cast after the voter last logged in.
        if v.time >= v.voter.last_login - one_day:
            v.time = v.voter.last_login + one_hour
        # If the time now places the dent out of order, force it forward.
        if v.time <= last_allocated_time:
            v.time = last_allocated_time + one_hour
        last_allocated_time = v.time
        v.save()

def create_reputations():
    # This simulates all denting and voting actions "in-order" to determine
    # what the user reputations would have been. If a vote would not have
    # been allowed, it is deleted.
    dent_content_type = ContentType.objects.get_for_model(Dent)
    vote_content_type = ContentType.objects.get_for_model(Vote)
    # Initialize user reputation
    for u in User.objects.all():
        Reputation.objects.update_user_reputation(u, 0)
    # Initialize current dent and vote
    current_dent = Dent.objects.order_by('time')[:1].get()
    current_vote = Vote.objects.order_by('time')[:1].get()
    while current_dent or current_vote:
        if current_dent and ((current_vote and current_dent.time < current_vote.time) or not current_vote):
            # Process a dent
            print "Processing dent %d" % current_dent.id
            reputation_action = ReputationAction(user = current_dent.poster,
                                                 originating_user = current_dent.poster,
                                                 content_type = dent_content_type,
                                                 object_id = current_dent.id,
                                                 value = DentingReputationHandler.CREATE_VALUE)
            reputation_action.save()
            reputation_action.date_created = current_dent.time
            reputation_action.save()
            Reputation.objects.update_reputation(reputation_action.user, reputation_action.value)
            # Fetch the next dent
            try:
                current_dent = Dent.objects.filter(time__gt=current_dent.time).order_by('time')[:1].get()
            except Dent.DoesNotExist:
                current_dent = None
        elif current_vote:
            current_vote_time = current_vote.time # In case it gets deleted
            # Process a vote
            print "Processing vote %d" % current_vote.id
            if (current_vote.value == 1 and current_vote.voter.reputation.reputation < settings.REPUTATION_PERMISSONS['can_vote_up']) \
              or (current_vote.value == -1 and current_vote.voter.reputation.reputation < settings.REPUTATION_PERMISSONS['can_vote_down']):
                print "Vote not allowed, deleting..."
                current_vote.delete()
            else:
                vote_value = VotingReputationHandler.UP_VALUE if current_vote.value == 1 else VotingReputationHandler.DOWN_VALUE
                reputation_action = ReputationAction(user = current_vote.dent.poster,
                                                     originating_user = current_vote.voter,
                                                     content_type = vote_content_type,
                                                     object_id = current_vote.id,
                                                     value = vote_value)
                reputation_action.save()
                reputation_action.date_created = current_vote.time
                reputation_action.save()
                Reputation.objects.update_reputation(reputation_action.user, reputation_action.value)
            # Fetch the next vote
            try:
                current_vote = Vote.objects.filter(time__gt=current_vote_time).order_by('time')[:1].get()
            except Vote.DoesNotExist:
                current_vote = None
    # Fix vote sums after vote deletions
    for d in Dent.objects.all():
        vote_sum = d.vote_set.aggregate(Sum('value'))['value__sum']
        d.vote_sum = vote_sum or 0
        d.save()

def run_updates():
    print "Populating vote times..."
    populate_vote_time()
    print "Creating reputations..."
    create_reputations()
    print "Updating dents..."
    for d in Dent.objects.order_by('time'):
        update_content(d)
    print "Done!"

def fix_last_featured():
    for d in Dent.objects.all():
        if not d.last_featured:
            d.last_featured = datetime.datetime.utcfromtimestamp(0)
        d.save()

def fill_dent_langs():
    for d in Dent.objects.all():
        lang_text = striptags(d.text)
        lang_text = urlizetrunc(lang_text, 0)
        lang_text = striptags(lang_text)
        lang_code = guess_language(lang_text)
        if lang_code:
            # If the poster's language matches then use it.
            if d.poster.sitesettings.language and d.poster.sitesettings.language.startswith(lang_code):
                d.language = d.poster.sitesettings.language
            else:
                # Search the Django languages for matches
                lang_codes = [lang[0] for lang in settings.LANGUAGES if lang[0].startswith(lang_code)]
                if lang_codes:
                    # Use the first match.
                    d.language = lang_codes[0]
                else:
                    # WTF no match? Just use the code directly.
                    d.language = lang_code
        else:
            # Default to the poster's language, or English if not set.
            if d.poster.sitesettings.language:
                d.language = d.poster.sitesettings.language
            else:
                d.language = 'en'
        d.save()
