from django.conf.urls import url

from ident.dent.models import Dent
from ident.dent.views import dent_delete, dent_vote,\
                       privatedent_delete,\
                       DentDetailView, DentCreateView, DentEditView,\
                       PrivateDentDetailView, PrivateDentCreateView, PrivateDentEditView
from ident.user.views import user_follow

urlpatterns = [
    # create new dent
    url(r'^new/?$', DentCreateView.as_view(), name='dent_create_view'),
    url(r'^(?P<pk>\d+)/?$', DentDetailView.as_view(), name='dent_detail_view'),
    # reply to a dent
    url(r'^(?P<replyto_id>\d+)/reply/?$', DentCreateView.as_view(),
        name='dent_reply_view'),
    url(r'^(?P<dent_id>\d+)/edit/?$', DentEditView.as_view(),
        name='dent_edit'),
    url(r'^(?P<dent_id>\d+)/delete/?$', dent_delete,
        name='dent_delete'),
    url(r'^(?P<dent_id>\d+)/stalk/?$', user_follow),
    url(r'^(?P<dent_id>\d+)/vote(/up)?/?$', dent_vote, {'down': False},
        name='dent_vote_up'),
    url(r'^(?P<dent_id>\d+)/vote/down/?$', dent_vote, {'down': True},
        name='dent_vote_down'),
    # private dents
    url(r'^p/new/?$', PrivateDentCreateView.as_view(), name='privatedent_create_view'),
    url(r'^p/(?P<pk>\d+)/?$', PrivateDentDetailView.as_view(), name='privatedent_detail_view'),
    url(r'^p/(?P<pdent_id>\d+)/edit/?$', PrivateDentEditView.as_view(),
        name='privatedent_edit'),
    url(r'^p/(?P<pdent_id>\d+)/delete/?$', privatedent_delete,
        name='privatedent_delete'),
]
