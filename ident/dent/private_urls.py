from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from ident.dent.views import PrivateDentListView, PrivateDentUserListView, UserPrivateDentListView, FeedbackPrivateDentListView
from ident.search.views import submit_search

urlpatterns = [
    url(r'^\/?$', login_required(PrivateDentListView.as_view()), name="privatedent_list"),
    url(r'^u\/?$', login_required(PrivateDentUserListView.as_view()), name="privatedent_users_list"),
    url(r'^feedback/?$', login_required(FeedbackPrivateDentListView.as_view()), name="privatedent_feedback_list"),
    url(r'^u/by_username/?$', PrivateDentUserListView.as_view(),
        {'sort_by':'username'},
        name='privatedent_by_username'),
    url(r'^u/by_-username/?$', PrivateDentUserListView.as_view(),
        {'sort_by':'-username'},
        name='privatedent_by_-username'),
    url(r'^u/by_time/?$', PrivateDentUserListView.as_view(),
        {'sort_by':'time'},
        name='privatedent_by_time'),
    url(r'^u/by_-time/?$', PrivateDentUserListView.as_view(),
        {'sort_by':'-time'},
        name='privatedent_by_-time'),
    url(r'^u/(?P<username>[^/]+)/?$', login_required(UserPrivateDentListView.as_view()), name="privatedent_user_list"),
    url('^search/$', login_required(submit_search), {'searchpath': '/private/search/'}, "submit_privatedent_search"),
    url(r'^search/(?P<searchterm>.+)\/?$', login_required(PrivateDentListView.as_view()), name="privatedent_search_list"),
]
