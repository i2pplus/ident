from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.syndication.views import Feed
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.decorators.http import condition

from ident.dent.models import Dent

class LatestEntriesFeed(Feed):
    #Conditional HTTP: http://www.ietf.org/rfc/rfc3229.txt
    title = _("id3nt.i2p updates")
    link = "/"
    description = _("The last 30 dents from id3nt.i2p | Buzzing in your ear since 2010!")

    def __call__(self, request, *args, **kwargs):
        ifmodsince =  request.META.get('HTTP_IF_LAST_MODIFIED', None)
        if ifmodsince:
            # drop the timezone info
            ifmodsince = ifmodsince.rsplit(' ', 1)[0]
            try:
                self._ifmodsince = datetime.strptime(ifmodsince,
                               "%a, %d %b %Y %H:%M:%S")
            except ValueError:
                self._ifmodsince = None
        else:
            self._ifmodsince = None
        response = super(LatestEntriesFeed, self).__call__(request, *args, **kwargs)
        if self._ifmodsince and not self.items().count():
            response.status_code=304
        return response

    def items(self):
        if self._ifmodsince:
            return Dent.objects.filter(time__gt= self._ifmodsince).reverse()[:30]
        else:
            return Dent.objects.all()[:30]

    def item_title(self, item):
        return str(item)

    def item_description(self, item):
        return u"%s: %s" % (item.poster.username, item.text)

    def item_pubdate(self, item):
        return item.time


class LatestUserEntriesFeed(Feed):

    def get_object(self, request, username):
        self._user = get_object_or_404(User, username=username)
        return self._user

    def title(self, obj):
        return _("id3nt.i2p updates for %(username)s") % {'username': obj.username}

    def link(self, obj):
        return obj.get_absolute_url()

    def description(self, obj):
        return _("The last 30 dents by %(username)s") % {'username': obj.username}

    def author_name(self, obj):
        return obj.username

    def items(self, obj):
        return Dent.objects.filter(poster=obj)[:30]

    def item_title(self, item):
        return str(item)

    def item_description(self, item):
        return item.text

    def item_author_name(self):
        return self._user.username

    def item_pubdate(self, item):
        return item.time

class LatestUserStalkingFeed(LatestUserEntriesFeed):

    def title(self, obj):
        return _("%(username)s's stalk targets") % {'username': obj.username}

    def link(self, obj):
        return obj.get_absolute_url()+'/stalking/'

    def description(self, obj):
        return _("The last 30 dents by %(username)s's targets") % {'username': obj.username}

    def items(self, obj):
        f = User.objects.filter(followers__user=obj)
        return Dent.objects.filter(poster__in=f)[:30]

    def item_author_name(self, item):
        return item.poster.username
