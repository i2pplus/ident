try:
    import simplejson as json
except ImportError:
    import json
import datetime
import re

from django.forms import HiddenInput
from django.forms.models import model_to_dict
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Max, Q
from django import http
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.decorators.http import condition
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, UpdateView, DeleteView
from django.views.generic.detail import BaseDetailView, DetailView
from django.views.generic.edit import BaseCreateView, CreateView

from ident.dent.models import Dent, PrivateDent, Vote
from ident.dent.forms import DentForm, PrivateDentForm, FeedbackPrivateDentForm
from ident.views import IdentViewMixin

class DentDetailView(DetailView):
    """Show a single dent

    Adds in the dent.poster as context variable 'u'"""
    context_object_name = "dent"
    model = Dent
    template_name = 'dent/dent_detail.html'

    def get_queryset(self):
        queryset= super(DentDetailView, self).get_queryset()
        # Get an extra field with the current user's vote for this dent
        if self.request.user and self.request.user.id:
            queryset = queryset.extra(select={
            'user_vote': 'SELECT value FROM ident_vote WHERE ident_vote.dent_id = ident_dent.id AND ident_vote.voter_id = %d' % self.request.user.id
            })
        return queryset

    def get_context_data(self, **kwargs):
        # get original context
        context = super(DentDetailView, self).get_context_data(**kwargs)
        # Add in the user of the current dent
        context['u'] = self.object.poster
        return context


class PrivateDentDetailView(DentDetailView):
    """Show a single private dent

    Adds in the dent.poster as context variable 'u'"""
    context_object_name = "pdent"
    template_name = 'dent/privatedent_detail.html'

    def get_queryset(self):
        # Users can only see private dents that they sent or received
        return PrivateDent.objects.filter(Q(poster=self.request.user.id) | Q(rec_user=self.request.user.id))

    def get_context_data(self, **kwargs):
        """Add in the recipient"""
        # Call the base implementation first to get a context
        context = super(PrivateDentDetailView, self).get_context_data(**kwargs)
        # Add in the recipient for dents the user sends
        context['recipient'] = self.object.rec_user if self.object.poster == self.request.user else self.object.poster
        # Check that the recipient is subscribed to the user
        try:
            context['recipient'].profile.subscriptions.get(id=self.request.user.id)
        except User.DoesNotExist:
            # Warn the user that they cannot send private dents to the username
            messages.warning(self.request, _('You cannot send dents to this user. Make them stalk you!'))
        return context

    @method_decorator(auth.decorators.permission_required('dent.can_use_privatedents', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super(PrivateDentDetailView, self).dispatch(*args, **kwargs)


class JSONResponseMixin(object):
    """Returns a result as a json object.

    Classes deriving from this should implement
    convert_context_to_json(self, context) which returns a json object
    to be output."""
    def render_to_response(self, context):
        "Returns a JSON response containing 'context' as payload"
        status, content = self.convert_context_to_json(context)
        return self.get_json_response(content, status=status)

    def get_json_response(self, json_content, **httpresponse_kwargs):
        """"Construct an `HttpResponse` object containing json_content

        pass in further kwargs, e.g. status_code=403. or whatever"""
        return http.HttpResponse(json.dumps(json_content),
                                 content_type='application/json',
                                 **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        """Convert the context dictionary into a JSON object

        Return a (HTTP_STATUS_CODE, dict) that will be output as json
        string. By default, this will use context['object']. status
        code is 200 by default. Override this function if you need
        some customization."""
        jobj = model_to_dict(context.get('object', None))
        return 200, jobj


class JsonLastDentDetailView(BaseDetailView, JSONResponseMixin):
    """Fetches the last dent of a user and returns it as a json object"""
    def get_object(self):
        username =  self.kwargs.get('username')
        try:
            self.object = Dent.objects.filter(poster__username=username)\
                .latest()
        except Dent.DoesNotExist:
            self.object = None
        return self.object


    def convert_context_to_json(self, context):
        """Convert the context dictionary into a JSON object

        Return a dict that will be output as json string. By default,
        this will use self.object. Override this function if you
        need some customization."""
        st_code = 200
        if self.object is None:
            return (404, {'error': 'No such dent', 'err_code':404})
        jobj = model_to_dict(self.object, exclude=('votes','poster'))
        if jobj['in_reply_to'] is None:
           del jobj['in_reply_to']
        jobj['vote'] = self.object.vote_sum
        jobj['poster'] = self.object.poster.username
        return (st_code, jobj)


class DentEditView(UpdateView):
    """Allow user to edit an already posted dent"""
    model = Dent
    form_class = DentForm
    template_name = 'dent/dent_form.html'

    def get_form(self, form_class):
        form = super(DentEditView, self).get_form(form_class)
        # Suppress the language field label
        form.fields['language'].label = ''
        return form

    def get_object(self):
        """Return the dent object

        self.object has been populated in dispatch() already, so just
        return it here."""
        if self.object:
            return self.object

    def get_context_data(self, **kwargs):
        """Add in the replied_dent, so we can show the thread"""
        # Call the base implementation first to get a context
        context = super(DentEditView, self).get_context_data(**kwargs)
        # Add in the user of the current dent
        context['replied_dent'] = self.object.in_reply_to
        return context

    def dispatch(self, request, *args, **kwargs):
        """Check if we have permissions to delete a dent"""
        dent_id = kwargs.get('dent_id')
        self.object = get_object_or_404(Dent, id=dent_id)
        # Permission to delete?
        has_perms = self.object.poster == request.user or \
            request.user.has_perm('ident.change_dent')
        if not has_perms:
            messages.warning(request, _("You do not have the permission required to edit this dent."))
            #TODO, do something with http.HttpResponseForbidden here
            return redirect(self.object)
        return super(DentEditView, self).dispatch(
            request, *args, **kwargs)


class PrivateDentEditView(UpdateView):
    """Allow user to edit an already posted private dent"""
    model = PrivateDent
    form_class = PrivateDentForm
    template_name = 'dent/privatedent_form.html'

    def get_form(self, form_class):
        form = super(PrivateDentEditView, self).get_form(form_class)
        # Hide recipient choice field - the recipient is already known
        form.fields['rec_user'].widget = HiddenInput()
        return form

    def get_object(self):
        """Return the private dent object

        self.object has been populated in dispatch() already, so just
        return it here."""
        if self.object:
            return self.object

    def get_context_data(self, **kwargs):
        """Add in the recipient"""
        # Call the base implementation first to get a context
        context = super(PrivateDentEditView, self).get_context_data(**kwargs)
        # Add in the recipient of the current dent
        context['recipient'] = self.object.rec_user
        return context

    @method_decorator(auth.decorators.permission_required('dent.can_use_privatedents', raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        """Check if we have permissions to edit a private dent"""
        pdent_id = kwargs.get('pdent_id')
        self.object = get_object_or_404(PrivateDent, id=pdent_id)
        # Permission to delete?
        has_perms = self.object.poster == request.user or \
            request.user.has_perm('ident.change_privatedent')
        if not has_perms:
            messages.warning(request, _("You lack the requisite permission to edit this StalkMail."))
            #TODO, do something with http.HttpResponseForbidden here
            return redirect(self.object)
        return super(PrivateDentEditView, self).dispatch(
            request, *args, **kwargs)


class CommonDentCreateView(object):
    """Common ancestor to both the API and webui dent creation UI"""
    #use model _or_ define self.form_class
    #form_class = DentForm
    #model = Dent
    atatUser_re = re.compile('(^|\s)@@(?P<username>[@.\w-]+)\\b')

    def get_form_class(self):
        """If needed create a PrivateDent, rather than a Dent"""
        if self.request.method not in ('POST', 'PUT'):
            return DentForm #just getting the dent form
        postcopy = self.request.POST.copy()
        m = CommonDentCreateView.atatUser_re.search(postcopy['text'])
        if not m:
            return DentForm #just a normal dent
        try:
            rec_user = User.objects.get(username=m.group(2))
            self.kwargs['rec_user_id'] = rec_user.id
        except User.DoesNotExist:
            messages.warning(self.request, _("The user is unknown to us."))
            # Set rec_user_id to the user, so that the form validation fails
            self.kwargs['rec_user_id'] = self.request.user.id
        return PrivateDentForm

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instanciating the form.

        Overrides FormMixin.get_form_kwargs
        """
        # we need to override this function, so we can fudge the form input
        # values, e.g. by assigning the poster to be the authentication user
        kwargs = {'initial': self.get_initial()}
        if self.request.method not in ('POST', 'PUT'):
            return kwargs
        postcopy = self.request.POST.copy()
        # Fetch poster user
        postcopy['poster'] = self.request.user.id
        # set 'in_reply_to' if we get a 'replyto_id' keyword
        in_reply_to = self.kwargs.get('replyto_id', None)
        if in_reply_to:
            postcopy['in_reply_to'] = in_reply_to

        # set 'rec_user' if we get a 'rec_user_id' keyword
        # This only gets set by get_form_class() above, so
        # the form is guaranteed to be a PrivateDentForm.
        rec_user = self.kwargs.get('rec_user_id', None)
        if rec_user:
            postcopy['rec_user'] = rec_user

        kwargs.update({
           'data': postcopy,
           'files': self.request.FILES,
           })
        return kwargs

    def exceeded_ratelimit(self, request):
        """Implement Rate limiting

        Return True if the user exceeded its time limit for posting"""
        if request.user.has_perm('ident.new_user_restrictions_lifted'):
            # Allow 5 dents every 10 minutes
            sincetime = datetime.datetime.now() - datetime.timedelta(minutes=10)
            dent_count = Dent.objects.filter(poster=request.user, time__gt=sincetime).count()
            if dent_count >= 5:
                return True
        else:
            # Allow 2 dents every 24 hours
            sincetime = datetime.datetime.now() - datetime.timedelta(days=1)
            dent_count = Dent.objects.filter(poster=request.user, time__gt=sincetime).count()
            if dent_count >= 2:
                return True
        return False


class APIDentCreateView(CommonDentCreateView,
                        BaseCreateView, JSONResponseMixin):
    """Post a new dent via simple HTTP POST form"""

    def convert_context_to_json(self, context):
        """Convert the context dictionary into a JSON object"""
        jobj={}

        #check if we have been handed err_code and error and return those if yes.
        st_code = context.get('err_code', 200)
        if context.get('error', None):
            jobj = {'err_code': st_code, 'error': context.get('error')}
            return (st_code, jobj)

        #no errors,
        form = context.get('form', None)
        if form:
            if form.errors:
                st_code = 412
                jobj = {'err_code': st_code, 'error':'form contains errors','fields':{}}
                for field, error in form.errors.items():
                    jobj['fields'][field] = error
            else:
                jobj = {'err_code': st_code, 'result': "Posted: %s" % context['form']}
        return (st_code, jobj)

    def form_valid(self, form):
        """Rather than redirecting to SuccessUrl we want to output the success text"""
        #returns HttpResponseRedirect to get_success_url() and sets self.object
        super(APIDentCreateView, self).form_valid(form)
        return self.render_to_response(self.get_context_data())

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        """override dispatch with a csrf_excempt decorator

        Authenticate the user via simple POST values 'user', 'password'"""
        #check authentication. TODO factor out:
        username = request.POST.get('user', None)
        passwd = request.POST.get('password', None)
        #TODO, handle non-existing
        request.user = auth.authenticate(username=username,
                                              password=passwd)
        if request.user is None or not request.user.is_active:
            # Auth failed, bail out right here
            return self.render_to_response(
                {'err_code': 401,
                 'error': 'Authentication failed'})
        if self.exceeded_ratelimit(request):
            return self.render_to_response(
                {'err_code': 403,
                 'error': 'Cannot post twice within 3 Minutes.'})
        return super(APIDentCreateView, self).dispatch(request, *args, **kwargs)


class DentCreateView(CommonDentCreateView, CreateView):
    """Post a new dent via Webui"""
    template_name = 'dent/dent_form.html'

    def get_form(self, form_class):
        form = super(DentCreateView, self).get_form(form_class)
        # If this is a private dent:
        if form.fields.get('rec_user'):
            # Hide recipient choice field - the recipient is already known
            form.fields['rec_user'].widget = HiddenInput()
        # If this is a public dent:
        if form.fields.get('language'):
            # Suppress the language field label
            form.fields['language'].label = ''
        return form

    def get_success_url(self):
        """Return URLto redirect to after success

        use location specified with ?next= or POST 'next' and fall
        back dent detail view."""
        next = self.request.POST.get('next', None)
        if not next:
            next = self.request.GET.get('next', None)
        if next:
            return next
        return super(DentCreateView, self).get_success_url()

    def get_context_data(self, **kwargs):
        """If passed in a kwargs['replyto_id'], add the replied_dent context"""
        # Call the base implementation first to get a context
        context = super(DentCreateView, self).get_context_data(**kwargs)

        #add 'replied_dent' context if we actually reply to a dent
        replyto_id = self.kwargs.get('replyto_id', None)
        if replyto_id:
            try:
                context['replied_dent'] = Dent.objects.get(id=replyto_id)
            except Dent.DoesNotExist:
                pass
        return context

    def form_valid(self, form):
        #returns HttpResponseRedirect to get_success_url() and sets self.object
        res = super(DentCreateView, self).form_valid(form)
        success_msg = _('Posted new <a href="%(url)s">dent %(id)d</a>')
        # Change the message if a private dent was created
        rec_user = self.kwargs.get('rec_user_id', None)
        if rec_user:
            success_msg = _('Posted new <a href="%(url)s">StalkMail %(id)d</a>')
        messages.info(self.request, success_msg %
                      {'url': self.object.get_absolute_url(), 'id': self.object.id})
        return res

    def form_invalid(self, form):
        messages.warning(self.request, _('Dent was not valid.'))
        return super(DentCreateView, self).form_invalid(form)

    def get(self, request, *args, **kwargs):
        """Override to provide initial text in dentarea if this is a reply"""
        replyto_id = self.kwargs.get('replyto_id', None)
        if replyto_id:
            #This is a reply, so prepopulate webform with '@user' text
            try:
                reply = Dent.objects.get(id=replyto_id)
            except Dent.DoesNotExist:
                pass
            else:
                self.initial = {'text':'@%s ' % reply.poster.username}
        return super(DentCreateView, self).get(request, *args, **kwargs)

    @method_decorator(auth.decorators.login_required)
    def dispatch(self, request, *args, **kwargs):
        """override dispatch to require login"""
        self.request = request
        if self.exceeded_ratelimit(request):
            if request.user.has_perm('ident.new_user_restrictions_lifted'):
                messages.warning(request,
                _("Exceeded rate limit. Not posting dent. Hold your horses for a while."))
            else:
                messages.warning(request,
                _("You're a new user and haven't dented enough yet, so you can only post twice a day."))
            return redirect(self.get_success_url())
        return super(DentCreateView, self).dispatch(
            request, *args, **kwargs)


class PrivateDentCreateView(DentCreateView):
    """Post a new private dent via Webui"""
    template_name = 'dent/privatedent_form.html'

    def get_form_class(self):
        postcopy = self.request.POST.copy()
        if postcopy.has_key('text'):
            m = CommonDentCreateView.atatUser_re.search(postcopy['text'])
        else:
            m = None
        if m:
            # The user has specified a recipient, so verify it
            try:
                rec_user = User.objects.get(username=m.group(2))
                self.kwargs['rec_user_id'] = rec_user.id
            except User.DoesNotExist:
                messages.warning(self.request, _("Unknown user."))
                # Set rec_user_id to the user, so that the form validation fails
                self.kwargs['rec_user_id'] = self.request.user.id
        return PrivateDentForm

    def get_form(self, form_class):
        # Override DentCreateView.get_form() so that rec_user is not hidden
        form = super(DentCreateView, self).get_form(form_class)
        # Only allow recipients who are subscribers to the user
        form.fields['rec_user'].queryset = User.objects.filter(profile__subscriptions__id__exact=self.request.user.id)
        # Suppress the rec_user field label
        form.fields['rec_user'].label = ''
        return form

    def form_valid(self, form):
        #returns HttpResponseRedirect to get_success_url() and sets self.object
        # Override DentCreateView.form_valid() to ensure correct success msg
        res = super(DentCreateView, self).form_valid(form)
        success_msg = _('Posted new <a href="%(url)s">StalkMail %(id)d</a>')
        messages.info(self.request, success_msg % {'url': self.object.get_absolute_url(), 'id': self.object.id})
        return res

    def exceeded_ratelimit(self, request):
        """Implement Rate limiting

        Return True if the user exceeded its time limit for posting"""
        #allow 5 dents every 10 minutes
        sincetime = datetime.datetime.now() - datetime.timedelta(minutes=10)
        dent_count = PrivateDent.objects.filter(poster=request.user, time__gt=sincetime).count()
        if dent_count >= 5:
            return True
        return False

    @method_decorator(auth.decorators.permission_required('dent.can_use_privatedents', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super(PrivateDentCreateView, self).dispatch(*args, **kwargs)


class FeedbackPrivateDentCreateView(PrivateDentCreateView):
    """Post feedback to the admins"""
    template_name = 'dent/privatedent_feedback.html'

    def get_form_class(self):
        # This just needs a number to be valid.
        # The actual recipients are set in the form.
        self.kwargs['rec_user_id'] = 1
        return FeedbackPrivateDentForm

    def get_form(self, form_class):
        return super(DentCreateView, self).get_form(form_class)

    def get_success_url(self):
        """Return URLto redirect to after success

        use location specified with ?next= or POST 'next' and fall
        back to the index."""
        next = self.request.POST.get('next', None)
        if not next:
            next = self.request.GET.get('next', None)
        if next:
            return next
        return reverse('index')

    def form_valid(self, form):
        #returns HttpResponseRedirect to get_success_url() and sets self.object
        # Override DentCreateView.form_valid() to ensure correct success msg
        res = super(DentCreateView, self).form_valid(form)
        success_msg = _('Feedback successfully sent to the admins.')
        messages.info(self.request, success_msg)
        return res

    def exceeded_ratelimit(self, request):
        """Implement Rate limiting

        Return True if the user exceeded its time limit for posting"""
        # Allow 1 dent every 24 hours - this ensures that the user cannot
        # use the feedback form within the same day unless all admins clear
        # their private dents.
        sincetime = datetime.datetime.now() - datetime.timedelta(days=1)
        admins = [User.objects.get(username=u) for u in settings.FEEDBACK_ADMINS]
        dent_count = PrivateDent.objects.filter(poster=request.user,
                                                rec_user__in=admins,
                                                text__startswith='FEEDBACK:',
                                                time__gt=sincetime).count()
        if dent_count >= 1:
            return True
        return False

    def dispatch(self, request, *args, **kwargs):
        # Override PrivateDentCreateView to not require permissions
        self.request = request
        if self.exceeded_ratelimit(request):
            messages.warning(request,
            _("Exceeded rate limit. Not posting feedback. Hold your horses for a while."))
            return redirect(self.get_success_url())
        return super(DentCreateView, self).dispatch(
            request, *args, **kwargs)


class DentListView(IdentViewMixin, ListView):
    """This class provides a list of dents

    Customize with template_name and queryset options.
    It looks for a <hashtag> keyword argument passed in via
    URLpattern, returning only matching dents"""
    model = Dent
    context_object_name = "dent_list"
    paginate_by=30
    template_name='homepage.html'

    def latest_entry(self, request, *args, **kwargs):
        """datetime of latest dent in list"""
        if request.user.is_authenticated():
            return None
        try:
            return self.get_queryset().latest().time
        except self.model.DoesNotExist:
            return None

    def head(self, request, *args, **kwargs):
        """Return empty doc on HEAD requests

        #Some status detector spiders perform HEADs"""
        response = http.HttpResponse()
        date = self.latest_entry(request)
        if date:
            response['Last-Modified'] = date.strftime("%a, %d %b %Y %H:%M:%S GMT")
        return response

    def get(self, request, *args, **kwargs):
        response = super(DentListView, self).get(request, *args, **kwargs)
        date = self.latest_entry(request)
        if date:
            response['Last-Modified'] = date.strftime("%a, %d %b %Y %H:%M:%S GMT")
        return response

    def search_queryset(self, queryset):
        if self.kwargs.has_key('hashtag'):
            queryset= queryset.filter(
                text__icontains="#%s" % self.kwargs['hashtag'])
            self.page_title=_("Search results for '%(term)s'") % {'term': self.kwargs['hashtag']}
        elif self.kwargs.has_key('searchterm'):
            queryset= queryset.filter(
                text__icontains="%s" % self.kwargs['searchterm'])
            self.page_title=_("Search results for '%(term)s'") % {'term': self.kwargs['searchterm']}
        return queryset

    def sort_queryset_by(self, queryset):
        sort_by= self.kwargs.get('sort_by', None)
        if sort_by == 'vote':
            queryset= queryset.order_by('-damped_popularity', '-time')
            if hasattr(self, '_user'): #It's a user's list
                self.page_title = _("%(user)s's sickest dents") % {'user': self._user.username}
            else: self.page_title=_("Top Quality Dents")
        elif sort_by == '-vote':
            queryset= queryset.order_by('damped_popularity', '-time')
            if hasattr(self, '_user'): #It's a user's list
                self.page_title = _("%(user)s's failest dents") % {'user': self._user.username}
            else: self.page_title=_("Accidents")
        elif sort_by == 'time':
            queryset= queryset.order_by('time')
            if hasattr(self, '_user'): #It's a user's list
                self.page_title = _("%(user)s's crustiest dents") % {'user': self._user.username}
            else: self.page_title=_("Crustiest dents")
        return queryset

    def extra_queryset(self, queryset):
        # Get an extra field with the current user's vote for this dent
        if self.request.user and self.request.user.id:
            queryset = queryset.extra(select={
            'user_vote': 'SELECT value FROM ident_vote WHERE ident_vote.dent_id = ident_dent.id AND ident_vote.voter_id = %d' % self.request.user.id
            })
        return queryset

    def get_queryset(self):
        self.page_title = _('Freshest Dents')
        # Call the base implementation
        queryset= super(DentListView, self).get_queryset()

        queryset = self.search_queryset(queryset)
        queryset = self.sort_queryset_by(queryset)
        queryset = self.extra_queryset(queryset)
        return queryset

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DentListView, self).get_context_data(**kwargs)
        # 'Freshest dents' or whatever
        context['title'] = self.page_title
        #add 'friends' if in stalk-o-vision so the template knows the diff
        if self.kwargs.get('stalkovision', False):
            context['friends']= True
        return context

    #@method_decorator(condition(last_modified_func=latest_entry))
    #def dispatch(self, *args, **kwargs):
    #    return super(DentListView, self).dispatch(*args, **kwargs)


class UserDentListView(DentListView):
    """Displays a list of dents from one user"""
    template_name='user/user.html'

    def get_context_data(self, **kwargs):
        context = super(UserDentListView, self).get_context_data(**kwargs)
        context['u'] = self._user
        # 'Freshest dents' or whatever
        context['title'] = self.page_title
        return context

    def get_queryset(self):
        # get the user whose home we show (we'll stuff him in the context too)
        self._user = get_object_or_404(User, username__exact=
                                         self.kwargs['username'])

        if self.kwargs.get('stalkovision', False):
            # User and their followers:
            f = User.objects.filter(Q(id=self._user.id) | Q(followers__user=self._user))
            queryset = Dent.objects.filter(poster__in=f)
            self.page_title = "%s's Stalk-o-vision" % self._user.username
        else:
            queryset = self.model.objects.filter(poster=self._user)
            self.page_title = self._user.username

        if self.kwargs.has_key('hashtag'):
            queryset= queryset.filter(
                text__icontains="#%s" % self.kwargs['hashtag'])
        elif self.kwargs.has_key('searchterm'):
            queryset= queryset.filter(
                text__icontains="%s" % self.kwargs['searchterm'])

        queryset = self.sort_queryset_by(queryset)
        queryset = self.extra_queryset(queryset)
        return queryset


class PrivateDentListView(DentListView):
    """This class provides a list of private dents

    Customize with template_name and queryset options.
    It looks for a <hashtag> keyword argument passed in via
    URLpattern, returning only matching dents"""
    model = PrivateDent
    context_object_name = "pdent_list"
    paginate_by=30
    template_name='dent/privatedent_homepage.html'

    def get(self, request, *args, **kwargs):
        # Override so private dent pages get refreshed
        return super(DentListView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        self.page_title = _('StalkMail')
        # Call the base implementation
        queryset = super(DentListView, self).get_queryset()

        # Filter out private dents that are not for this user
        queryset = queryset.filter(Q(poster=self.request.user.id) | Q(rec_user=self.request.user.id))

        queryset = self.search_queryset(queryset)
        queryset = self.sort_queryset_by(queryset)
        return queryset

    @method_decorator(auth.decorators.permission_required('dent.can_use_privatedents', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super(PrivateDentListView, self).dispatch(*args, **kwargs)


class UserPrivateDentListView(PrivateDentListView):
    """Displays a list of private dents with one user"""

    def get_context_data(self, **kwargs):
        """Add in the recipient"""
        # Call the base implementation first to get a context
        context = super(UserPrivateDentListView, self).get_context_data(**kwargs)
        # Update the page title
        context['title'] = self.page_title % {'user': self._recipient.username}
        # Add in the recipient for dents the user sends
        context['recipient'] = self._recipient
        # Check that the recipient is subscribed to the user
        try:
            self._recipient.profile.subscriptions.get(id=self.request.user.id)
        except User.DoesNotExist:
            # Warn the user that they cannot send private dents to the username
            messages.warning(self.request, _('You cannot send dents to this user. Make them stalk you!'))
        return context

    def get_queryset(self):
        self.page_title = _('StalkMail with %(user)s')
        queryset = super(UserPrivateDentListView, self).get_queryset()
        # get the user whose private dents we show (we'll stuff him in the context too)
        self._recipient = get_object_or_404(User, username__exact=
                                         self.kwargs['username'])
        queryset = queryset.filter(Q(poster=self._recipient) | Q(rec_user=self._recipient))
        return queryset

    @method_decorator(auth.decorators.permission_required('dent.can_use_privatedents', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super(UserPrivateDentListView, self).dispatch(*args, **kwargs)


class PrivateDentUserListView(PrivateDentListView):
    paginate_by = 10
    template_name = 'dent/privatedent_users.html'

    def sort_queryset_by(self, queryset):
        queryset = queryset.order_by('poster')
        sort_by = self.kwargs.get('sort_by', None)
        if sort_by is None:
            #default: by last activity
            queryset = queryset.order_by('time').reverse()
        elif sort_by in ['time', '-time']:
            queryset = queryset.order_by('time')
        elif sort_by in ['username', '-username']:
            queryset = queryset.order_by('poster__username')
        if sort_by and sort_by[0] == '-':
            queryset = queryset.reverse()
        return queryset

    def get_queryset(self):
        # Call the parent implementation to get the private dents for this user
        queryset = super(PrivateDentUserListView, self).get_queryset()
        # Only show received dents
        queryset = queryset.filter(rec_user=self.request.user)
        # Only show the latest dent from each poster
        latest_pdents = User.objects.annotate(latest_pdent=Max('privatedent__time')).values('id', 'latest_pdent')
        conjunc = map(lambda user: Q(poster=user['id']).__and__(Q(time=user['latest_pdent'])), latest_pdents)
        queryset = queryset.filter(reduce(lambda disjunc, x: disjunc.__or__(x), conjunc[1:], conjunc[0]))
        return queryset

    @method_decorator(auth.decorators.permission_required('dent.can_use_privatedents', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super(PrivateDentUserListView, self).dispatch(*args, **kwargs)


class FeedbackPrivateDentListView(PrivateDentListView):
    """Displays a list of feedback"""
    page_title=_('Feedback')

    def get_queryset(self):
        # Call the parent implementation to get the private dents for this user
        queryset = super(FeedbackPrivateDentListView, self).get_queryset()
        # Only show received dents
        queryset = queryset.filter(rec_user=self.request.user)
        # Only show feedback
        queryset = queryset.filter(text__startswith='FEEDBACK:')
        return queryset

    def dispatch(self, request, *args, **kwargs):
        if request.user.username in settings.FEEDBACK_ADMINS:
            return super(PrivateDentListView, self).dispatch(request, *args, **kwargs)
        return http.HttpResponseForbidden()


@auth.decorators.login_required
def dent_delete(request, dent_id):
    dent = get_object_or_404(Dent, id=dent_id)
    # Permission to delete?
    is_mine = dent.poster == request.user or \
        request.user.has_perm('ident.delete_dent')

    # object/url we want to redirect after this
    next = request.GET.get('next')
    if not next:
        next = request.user.get_absolute_url()

    if not is_mine:
        messages.warning(request, _("You can only delete your own dents, clown!"))
        return redirect(next)
    return DeleteView.as_view(
        model=Dent,
        success_url=next,
        template_name='dent/dent_confirm_delete.html'
    )(request,pk=dent_id, extra_context={'u': request.user})

@auth.decorators.login_required
@auth.decorators.permission_required('dent.can_use_privatedents', raise_exception=True)
def privatedent_delete(request, pdent_id):
    pdent = get_object_or_404(PrivateDent, id=pdent_id)
    # Permission to delete?
    is_mine = pdent.poster == request.user or \
        pdent.rec_user == request.user or \
        request.user.has_perm('ident.delete_privatedent')

    # object/url we want to redirect after this
    next = request.GET.get('next')
    if not next:
        next = reverse('privatedent_list')

    if not is_mine:
        messages.warning(request, _("Can not delete StalkMail which had "\
                         "not been posted by you or sent to you"))
        return redirect(next)
    return DeleteView.as_view(
        model=PrivateDent,
        success_url=next,
        template_name='dent/privatedent_confirm_delete.html'
    )(request, pk=pdent_id, extra_context={'u': request.user})

@auth.decorators.login_required
def dent_vote(request, dent_id, down=False):
    """Create/Update a dent vote

    A signal handler is in dent/models.py listening to changes to the
    m2m fields and updating dent.vote_sum when it has changed."""
    dent = get_object_or_404(Dent, id=dent_id)
    user = request.user

    sinceday = datetime.datetime.now() - datetime.timedelta(days=1)
    sinceweek = datetime.datetime.now() - datetime.timedelta(days=7)
    day_user_vote_count = Vote.objects.filter(voter=request.user, dent__poster=dent.poster, time__gt=sinceday).count()
    week_user_vote_count = Vote.objects.filter(voter=request.user, dent__poster=dent.poster, time__gt=sinceweek).count()
    if day_user_vote_count >= 3:
        messages.warning(request,
            _("You've already cast 3 votes on target's dents in the last 24 hours."))
    elif week_user_vote_count >= 10:
        messages.warning(request,
            _("You've already cast 10 votes on target's dents in the last week."))
    else:
        value = 1
        if down: value *= -1

        if value > 0 and not user.has_perm('ident.can_vote_up'):
            messages.warning(request, _("You don't have enough karma to vote dents up."))
        elif value < 0 and not user.has_perm('ident.can_vote_down'):
            messages.warning(request, _("You don't have enough karma to vote dents down."))
        else:
            if user == dent.poster:
                value = -1
                messages.warning(request, _("Clown Alert!!! You just dumped on your own dent! Well done!"))
            else:
                # Is someone elses dent
                if value > 0:
                    messages.info(request,_("Dent approval registered. Thanks for smiling on this dent!"))
                else:
                    messages.info(request,_("Disapproval duly noted. Thanks for making your displeasure plain!"))
            try:
                vote = Vote.objects.get(dent=dent, voter=user)
            except Vote.DoesNotExist:
                vote = Vote(dent=dent, voter=user)
            vote.value= value
            vote.save()

    #redirect to GET['next'] or the dent permaling next
    next = request.GET.get('next', None)
    if next:
        page = request.GET.get('page', None)
        if page:
            next += '?page=%s' % page
        return redirect(next)
    return redirect(dent)

