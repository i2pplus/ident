from __future__ import division
import datetime
import re

from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.dispatch import receiver
from django.template.defaultfilters import force_escape, striptags, urlizetrunc,\
    linebreaks, truncatewords
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from hashtags.signals import (hashtagged_model_was_saved,
                              hashtagged_model_was_deleted,
                              parse_fields_looking_for_hashtags,
                              handle_removal_of_hashtagged_model)
from hashtags.templatetags.hashtags_tags import urlize_hashtags
from reputation.handlers import BaseReputationHandler

from ident.dent.fields import LanguageField

hashtagged_model_was_saved.connect(parse_fields_looking_for_hashtags)
hashtagged_model_was_deleted.connect(handle_removal_of_hashtagged_model)

class AbstractDent(models.Model):
    text = models.CharField(max_length=1024)
    poster = models.ForeignKey(User, blank=True)
    time = models.DateTimeField(auto_now_add=True)

    #at_user_re = re.compile('^(?P<prefix>.*\s)?(?P<atname>@[\w\-]+)(?P<appendix>.*)$')

    # words longer than 25 alphanumerics, but only if not preceeded by
    # :// (url scheme)
    truncate_re = re.compile("""
    (^|\s)                         # either string start or after space
    (?P<longword>([^"\s#][^"\s]{12})      # 13 nonspaces, not containing ", not starting in #
                                   # (used for abbreviation), followed by
     ((?<!://)\S){13,}             # >=13 more nonspaces
    )
    \\b                            #end of alphanumeric word
    """, re.VERBOSE)
    atUser_re = re.compile('(^|\s)@(?P<username>[.+\w-]+[@.+\w-]*)\\b')

    def __unicode__(self):
        return u"%d (%s): %s" % (self.id,
                             self.poster.username,
                             truncatewords(self.text, 5))

    def get_follow_url(self):
        return self.get_absolute_url() + '/follow'

    def text_as_html(self):
        """Return the body of the dent as properly formatted html.

        This is done when viewed instead of by the form, so that dents
        can be edited by the user."""
        # escape HTML tags instead of stripping them
        text = force_escape(self.text)
        #text = striptags(self.text)
        if self.poster.has_perm('ident.new_user_restrictions_lifted'):
            text = urlizetrunc(text, 60)
        #disabled by dr|z3d: text = linebreaks(text)

        #find overly long words and truncate them
        text = Dent.truncate_re.sub('\\1<span class="truncated" title="\\g<longword>">[\\3&hellip;]</span>', text)

        # find @User and convert them to links
        text = Dent.atUser_re.sub('\\1<a href="/u/\\g<username>">@\\g<username></a> ', text)
        # find hashtags and convert them to links
        text = urlize_hashtags(text)
        return mark_safe(text)

    class Meta:
        abstract = True
        ordering = ('-time', )
        get_latest_by = 'time'


class Dent(AbstractDent):
    # The dent this dent is a reply to
    in_reply_to = models.ForeignKey('self', null=True, blank=True,
                                    related_name='replies', default=None)

    source = models.CharField(max_length=140, default='Web')

    # The language of the dent
    language = LanguageField(blank=True, default='en')

    # The votes for this dent
    votes = models.ManyToManyField(User, blank=True, through='Vote',
                                   related_name='votes')
    # this one is automatically updated when a vote occurs and reflects the
    # aggregated + and - votes.
    vote_sum = models.IntegerField(default=0, blank=False, editable=False)
    # this one is automatically updated when a vote occurs and reflects the
    # calculated popularity of the dent.
    popularity = models.DecimalField(default=0, blank=False, editable=False, max_digits=6, decimal_places=5)
    # this stores the calculated popularity of the dent damped by its age
    # and is updated when a vote occurs and by cron job
    damped_popularity = models.DecimalField(default=0, blank=False, editable=False, max_digits=6, decimal_places=5)

    # the last time the dent was featured as a dent-du-jour.
    last_featured = models.DateTimeField(default=datetime.datetime.utcfromtimestamp(0), blank=False, editable=False)
    # the number of times the dent has been featured as a dent-du-jour;
    # this is auto-incremented when a new dent-du-jour is chosen.
    times_featured = models.IntegerField(default=0, blank=False, editable=False)

    @models.permalink
    def get_absolute_url(self):
        return ('dent_detail_view', () ,{'pk': self.id})

    def get_reply_url(self):
        return self.get_absolute_url() + '/reply'

    def get_thread_list(self):
        """Returns direct replies to this thread in a list"""
        thread= [self]
        dent= self
        while dent.in_reply_to:
            thread.append(dent.in_reply_to)
            dent = dent.in_reply_to
        # Dents should be displayed newest-first
        #thread.reverse()
        return thread

    def calculate_popularity(self):
        """Calculate and return a dent's popularity.

        Invoked when a dent has been voted on. If the site becomes more
        popular, this should be moved to a cron job, and the dent should
        just be flagged for update when it is voted on."""
        # Calculate some parameters
        total_votes = Vote.objects.count() or 1
        total_dents = Dent.objects.count() or 1
        avg_num_votes = total_votes / total_dents
        total_vote_sum = Dent.objects.aggregate(models.Sum('vote_sum')).get('vote_sum__sum', 0)
        avg_vote = total_vote_sum / total_votes
        num_votes = self.vote_set.count()

        # Calculate the popularity with an adaptive Bayesian average
        popularity = (avg_num_votes * avg_vote) + self.vote_sum
        popularity = popularity / (avg_num_votes + num_votes)

        return popularity

    def calculate_damped_popularity(self):
        """Calculate and return the damped popularity of a dent.

        This is kept separate from the absolute popularity so that the more
        expensive/db-heavy popularity calculation only needs to be performed
        when a dent is voted on, while the lower-overhead age damping is
        performed daily for all dents.

        This value is stored rather than calculated on-the-fly because sorting
        the dents is much faster in the database than in Python (which would
        require fetching all Dents into memory and sorting them for every page
        request, regardless of how many would eventually be displayed)."""
        # Calculate the damped popularity
        if self.popularity > 0:
            age = datetime.datetime.now() - self.time
            age = age.days or 1
            damped_popularity = self.popularity / (age**0.02)
        else:
            damped_popularity = self.popularity

        return damped_popularity

    def update_popularity(self):
        """Update a dent's popularity and save it."""
        # Get the popularity of this dent
        popularity = self.calculate_popularity()

        # Save the popularity if it has changed
        if self.popularity != popularity:
            self.popularity = popularity
            self.save()

    def update_damped_popularity(self):
        """Update the damped popularity of a dent and save it.

        Invoked daily by a cron job."""
        # Get the damped popularity of this dent
        damped_popularity = self.calculate_damped_popularity()

        # Save the damped popularity if it has changed
        if self.damped_popularity != damped_popularity:
            self.damped_popularity = damped_popularity
            self.save()


# Capture Dent creation/modification signal and update user's karma
@receiver(models.signals.post_save, sender=Dent)
def dent_created_updated (sender, *args, **kwargs):
    dent = kwargs.get('instance')
    if kwargs.get('created'):
        dent.popularity = dent.calculate_popularity()
        dent.damped_popularity = dent.calculate_damped_popularity()
        dent.save()
        if not dent.poster.has_perm('ident.new_user_restrictions_lifted'):
            dent.poster.profile.check_new_user_restrictions()
    hashtagged_model_was_saved.send(sender=sender, instance=dent,
        hashtagged_field_list=['text'])
models.signals.post_save.connect(dent_created_updated, sender=Dent)

@receiver(models.signals.pre_delete, sender=Dent)
def dent_deleted(sender, instance, **kwargs):
    hashtagged_model_was_deleted.send(sender=sender, instance=instance)
models.signals.pre_delete.connect(dent_deleted, sender=Dent)


class DentingReputationHandler(BaseReputationHandler):
    model = Dent
    CREATE_VALUE = 1

    def check_conditions(self, instance):
        return True

    def remove_on_delete(self, instance):
        return True

    def get_target_object(self, instance):
        return instance

    def get_target_user(self, instance):
        return instance.poster

    def get_originating_user(self, instance):
        return instance.poster

    def get_value(self, instance):
        return self.CREATE_VALUE


class PrivateDent(AbstractDent):
    """Dents that target only a user or a group"""
    # receiver of the Private Message
    rec_user = models.ForeignKey(User, blank=False, null=False, related_name="received_privatedent_set", verbose_name="Recipient")

    @models.permalink
    def get_absolute_url(self):
        return ('privatedent_detail_view', () ,{'pk': self.id})

    def get_thread_list(self):
        """Returns private dents that give context to this one in a list"""
        thread = PrivateDent.objects.filter(
            models.Q(poster=self.poster) | models.Q(rec_user=self.poster),
            models.Q(poster=self.rec_user) | models.Q(rec_user=self.rec_user))
        # Show up to five private dents before this one
        oldest = thread.filter(time__lt=self.time).order_by('-time')[4:5]
        # Show up to two private dents after this one
        newest = thread.filter(time__gt=self.time).order_by('time')[1:2]
        try:
            thread = thread.filter(time__gte=oldest.get().time)
        except PrivateDent.DoesNotExist:
            pass
        try:
            thread = thread.filter(time__lte=newest.get().time)
        except PrivateDent.DoesNotExist:
            pass
        return thread

    class Meta:
        ordering = ('-time', )
        get_latest_by = 'time'
        permissions = (('can_use_privatedents', _('Can use StalkMail')),)


class Vote(models.Model):
    """Intermediary table for dent<->Vote<->User relationships"""
    voter = models.ForeignKey(User, blank=True)
    dent  = models.ForeignKey(Dent, blank=True)
    time = models.DateTimeField(auto_now=True)
    value = models.IntegerField(default=1,
                                help_text="+1 for up and -1 for downvoting")

    def __unicode__(self):
        return u"vote %+f on dent %s (%s)" % (self.value, self.dent.id,
                             self.voter.username)
    class Meta:
        ordering = ('dent','voter','value')
        permissions = (('can_vote_up', _('Can vote up')),
                       ('can_vote_down', _('Can vote down')))

# Capture VOTE creation/modification signal and update vote_sum
# signal that adds a 'message' when the user logged in
@receiver(models.signals.post_save, sender=Vote)
def vote_changed (sender, *args, **kwargs):
    vote = kwargs.get('instance')
    dent = vote.dent
    vote_sum = dent.vote_set.aggregate(models.Sum('value'))['value__sum']
    dent.vote_sum = vote_sum or 0
    dent.popularity = dent.calculate_popularity()
    dent.damped_popularity = dent.calculate_damped_popularity()
    dent.save()
    if not dent.poster.has_perm('ident.new_user_restrictions_lifted'):
        dent.poster.profile.check_new_user_restrictions()
models.signals.post_save.connect(vote_changed, sender=Vote)
#TODO, treat delete vote case too!


class VotingReputationHandler(BaseReputationHandler):
    model = Vote
    UP_VALUE = 10
    DOWN_VALUE = -10

    def check_conditions(self, instance):
        return True

    def has_changed(self, current, instance):
        return current.value != instance.value

    def get_target_object(self, instance):
        return instance

    def get_target_user(self, instance):
        return instance.dent.poster

    def get_originating_user(self, instance):
        return instance.voter

    def get_value(self, instance):
        value = 0
        if instance.value == 1:
            value = self.UP_VALUE
        elif instance.value == -1:
            value = self.DOWN_VALUE
        return value
