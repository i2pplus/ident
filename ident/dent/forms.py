import django.forms as forms
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import striptags, urlizetrunc
from django.utils.translation import ugettext as _
from guess_language import guess_language

from ident.dent.models import Dent, PrivateDent

class DentTextMixin(object):
    language = ''

    def clean_text(self):
        text = self.cleaned_data['text']
        if len(text) > 1024:
            raise forms.ValidationError("Ensure this value has at most 1024 characters (it has %d)." % len(text))
        # Strip all HTML tags and urls from the text for language guessing
        lang_text = striptags(text)
        lang_text = urlizetrunc(lang_text, 0)
        lang_text = striptags(lang_text)
        # Guess the language of the dent
        self.language = guess_language(lang_text)
        # find @User and convert them to links
        matches = Dent.atUser_re.findall(text)
        for (space, username) in matches:
            try:
                User.objects.get(username=username)
            except User.DoesNotExist:
                try:
                    matchedUser = User.objects.get(username__startswith=username)
                except User.DoesNotExist:
                    raise forms.ValidationError(_("%(username)s isn't someone we have the pleasure of knowing. Perhaps you'd like to invite them to join the party? Or check ur speeling.") % {'username': username})
                except User.MultipleObjectsReturned:
                    raise forms.ValidationError(_("@%(username)s matches several users; type the username fully.") % {'username': username})

        return text

class DentForm(DentTextMixin, forms.ModelForm):
    #TODO: need to suppress the "Text:" label in the form
    text = forms.CharField(label='', widget=forms.Textarea(
            attrs={'cols': 60, 'rows': 3}))
    class Meta:
        model = Dent
        exclude = ('source', 'votes','vote_sum','popularity','last_featured','times_featured')
        widgets = {'poster': forms.HiddenInput,
                   'in_reply_to': forms.HiddenInput}

    def clean(self):
        cleaned_data = super(DentForm, self).clean()
        # Only use the guessed language if not explicitly set.
        if not cleaned_data['language']:
            poster = cleaned_data.get('poster')
            if self.language:
                # If the poster's language matches then use it.
                if poster.sitesettings.language and poster.sitesettings.language.startswith(self.language):
                    cleaned_data['language'] = poster.sitesettings.language
                else:
                    # Search the Django languages for matches
                    lang_codes = [lang[0] for lang in settings.LANGUAGES if lang[0].startswith(self.language)]
                    if lang_codes:
                        # Use the first match.
                        cleaned_data['language'] = lang_codes[0]
                    else:
                        # WTF no match? Just use the code directly.
                        cleaned_data['language'] = self.language
            else:
                # Default to the poster's language, or English if not set.
                if poster.sitesettings.language:
                    cleaned_data['language'] = poster.sitesettings.language
                else:
                    cleaned_data['language'] = 'en'
        return cleaned_data

class PrivateDentForm(DentTextMixin, forms.ModelForm):
    #TODO: need to suppress the "Text:" label in the form
    text = forms.CharField(label='', widget=forms.Textarea(
            attrs={'cols': 60, 'rows': 3}))
    class Meta:
        model = PrivateDent
        fields = ('text', 'poster', 'rec_user')
        widgets = {'poster': forms.HiddenInput}

    def clean_rec_user(self):
        # Don't allow the recipient to change for an existing private dent
        if self.instance and self.instance.pk:
            return self.instance.rec_user
        else:
            return self.cleaned_data['rec_user']

    def clean(self):
        cleaned_data = super(PrivateDentForm, self).clean()
        poster = cleaned_data.get('poster')
        recipient = cleaned_data.get('rec_user')
        # Validation error for a blank recipient is raised elsewhere
        if recipient:
            try:
                recipient.profile.subscriptions.get(id=poster.id)
            except User.DoesNotExist:
                raise forms.ValidationError(_("You can send StalkMail to your stalking fanbase only. Sorry!"))

        # Everything checks out, so return the cleaned data
        return cleaned_data

class FeedbackPrivateDentForm(PrivateDentForm):
    class Meta:
        model = PrivateDent
        fields = ('text', 'poster', 'rec_user')
        widgets = {'poster': forms.HiddenInput,
                   'rec_user': forms.HiddenInput}

    def clean_rec_user(self):
        # Set the recipient as the primary admin.
        admin = User.objects.get(username=settings.FEEDBACK_ADMINS[0])
        self.cleaned_data['rec_user'] = admin
        return super(FeedbackPrivateDentForm, self).clean_rec_user()

    def clean_text(self):
        text = super(FeedbackPrivateDentForm, self).clean_text()
        return "FEEDBACK: %s" % text

    def clean(self):
        cleaned_data = super(PrivateDentForm, self).clean()
        return cleaned_data

    def save(self):
        # Save for the primary admin
        saved = super(FeedbackPrivateDentForm, self).save()
        # Duplicate to the secondary admins
        text = self.cleaned_data['text']
        if len(settings.FEEDBACK_ADMINS) > 1:
            for username in settings.FEEDBACK_ADMINS[1:]:
                admin = User.objects.get(username=username)
                feedback = PrivateDent(text=text,
                                       poster=self.cleaned_data['poster'],
                                       rec_user=admin)
                feedback.save()
        return saved
