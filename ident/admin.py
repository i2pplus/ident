from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from ident.dent.models import Dent, PrivateDent, Vote
from ident.user.models import Profile, SiteSettings

class DentAdmin(admin.ModelAdmin):
    pass
class PrivateDentAdmin(admin.ModelAdmin):
    pass
class VoteAdmin(admin.ModelAdmin):
    pass

admin.site.register(Dent, DentAdmin)
admin.site.register(PrivateDent, PrivateDentAdmin)
admin.site.register(Vote, VoteAdmin)


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False

class SiteSettingsInline(admin.StackedInline):
    model = SiteSettings
    can_delete = False

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (ProfileInline, SiteSettingsInline)

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
