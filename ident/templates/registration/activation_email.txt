Howdie,

someone attempts to create a user account over at our site {{site}} with
your email address.

If that someone is you, please click this link 

http://id3nt.i2p/accounts/activate/{{activation_key}} 

which will be valid for the next {{expiration_days}} days.

If that someone wasn't you, just ignore this link or let me know at str4d@mail.i2p.

Thank you for using ident.i2p
str4d
