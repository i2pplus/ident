from django.conf import settings

def translated_languages(request):
    return {'TRANSLATED_LANGUAGES': settings.TRANSLATED_LANGUAGES}
