from django import template
from django.contrib.staticfiles.templatetags.staticfiles import static

from random import randint

register = template.Library()

@register.simple_tag(takes_context=True)
def statictheme(context, path):
    """
    Returns the static URL for the specified theme-specific file.
    """
    # Change theme via query parameter for now; fetch it from context['user'] later.
    if context.has_key('request') and context['request'].GET.has_key('theme'):
        theme = context['request'].GET['theme']
    elif context.has_key('request') and context['request'].user.is_authenticated() and context['request'].user.sitesettings.theme:
        theme = context['request'].user.sitesettings.theme
    else:
        theme = 'burgundy'
    themed_path = 'themes/%s/%s' % (theme, path)
    return static(themed_path)
