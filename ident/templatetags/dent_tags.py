from datetime import datetime, timedelta
from django import template
from django.contrib.contenttypes.models import ContentType
from django.db.models import Count

from hashtags.models import Hashtag, HashtaggedItem

from ident.dent.models import Dent

register = template.Library()

def include_latest_tagcloud():
    now = datetime.now()
    one_week = timedelta(days=7)
    latest_dents = Dent.objects.filter(time__gte=(now-one_week))
    ctype = ContentType.objects.get_for_model(Dent)
    hashtagged_items = HashtaggedItem.objects.filter(content_type__pk=ctype.id, object_id__in=latest_dents)
    return {'hashtags': Hashtag.objects.filter(hashtaggeditem__id__in=hashtagged_items).order_by('name').annotate(Count('hashtaggeditem'))}

register.inclusion_tag('inc/tagcloud.html')(include_latest_tagcloud)
