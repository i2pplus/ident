#  Based on: http://www.djangosnippets.org/snippets/73/
#
#  Modified by Sean Reifschneider to be smarter about surrounding page
#  link context.  For usage documentation see:
#
#     http://www.tummy.com/Community/Articles/django-pagination/

from django import template

register = template.Library()

def paginator(context, adjacent_pages=3):
    """
    To be used in conjunction with the object_list generic view.

    Adds pagination context variables for use in displaying first, adjacent and
    last page links in addition to those created by the object_list generic
    view.

    """
    page_obj = context['page_obj']
    paginator = context['paginator']
    startPage = page_obj.number - adjacent_pages
    if startPage <= 2: startPage = 1
    endPage = page_obj.number + adjacent_pages + 1
    if endPage >= paginator.num_pages - 1: endPage = paginator.num_pages + 1
    page_numbers = range(startPage, endPage)
    return {
        'page_obj': context['page_obj'],
        'paginator': context['paginator'],
        'page_numbers': page_numbers,
        'has_previous': page_obj.number > startPage,
        'has_next': page_obj.number < paginator.num_pages,
        'show_first': 1 not in page_numbers,
        'show_last': paginator.num_pages not in page_numbers,
    }
register.inclusion_tag('inc/paginator.html', takes_context=True)(paginator)
