# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import ident.dent.fields
import ident.user.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Dent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=1024)),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('language', ident.dent.fields.LanguageField(default=b'en', max_length=7, blank=True, choices=[(b'af', b'Afrikaans'), (b'ar', b'Arabic'), (b'ast', b'Asturian'), (b'az', b'Azerbaijani'), (b'bg', b'Bulgarian'), (b'be', b'Belarusian'), (b'bn', b'Bengali'), (b'br', b'Breton'), (b'bs', b'Bosnian'), (b'ca', b'Catalan'), (b'cs', b'Czech'), (b'cy', b'Welsh'), (b'da', b'Danish'), (b'de', b'German'), (b'el', b'Greek'), (b'en', b'English'), (b'en-au', b'Australian English'), (b'en-gb', b'British English'), (b'eo', b'Esperanto'), (b'es', b'Spanish'), (b'es-ar', b'Argentinian Spanish'), (b'es-mx', b'Mexican Spanish'), (b'es-ni', b'Nicaraguan Spanish'), (b'es-ve', b'Venezuelan Spanish'), (b'et', b'Estonian'), (b'eu', b'Basque'), (b'fa', b'Persian'), (b'fi', b'Finnish'), (b'fr', b'French'), (b'fy', b'Frisian'), (b'ga', b'Irish'), (b'gl', b'Galician'), (b'he', b'Hebrew'), (b'hi', b'Hindi'), (b'hr', b'Croatian'), (b'hu', b'Hungarian'), (b'ia', b'Interlingua'), (b'id', b'Indonesian'), (b'io', b'Ido'), (b'is', b'Icelandic'), (b'it', b'Italian'), (b'ja', b'Japanese'), (b'ka', b'Georgian'), (b'kk', b'Kazakh'), (b'km', b'Khmer'), (b'kn', b'Kannada'), (b'ko', b'Korean'), (b'lb', b'Luxembourgish'), (b'lt', b'Lithuanian'), (b'lv', b'Latvian'), (b'mk', b'Macedonian'), (b'ml', b'Malayalam'), (b'mn', b'Mongolian'), (b'mr', b'Marathi'), (b'my', b'Burmese'), (b'nb', b'Norwegian Bokmal'), (b'ne', b'Nepali'), (b'nl', b'Dutch'), (b'nn', b'Norwegian Nynorsk'), (b'os', b'Ossetic'), (b'pa', b'Punjabi'), (b'pl', b'Polish'), (b'pt', b'Portuguese'), (b'pt-br', b'Brazilian Portuguese'), (b'ro', b'Romanian'), (b'ru', b'Russian'), (b'sk', b'Slovak'), (b'sl', b'Slovenian'), (b'sq', b'Albanian'), (b'sr', b'Serbian'), (b'sr-latn', b'Serbian Latin'), (b'sv', b'Swedish'), (b'sw', b'Swahili'), (b'ta', b'Tamil'), (b'te', b'Telugu'), (b'th', b'Thai'), (b'tr', b'Turkish'), (b'tt', b'Tatar'), (b'udm', b'Udmurt'), (b'uk', b'Ukrainian'), (b'ur', b'Urdu'), (b'vi', b'Vietnamese'), (b'zh-cn', b'Simplified Chinese'), (b'zh-hans', b'Simplified Chinese'), (b'zh-hant', b'Traditional Chinese'), (b'zh-tw', b'Traditional Chinese')])),
                ('vote_sum', models.IntegerField(default=0, editable=False)),
                ('popularity', models.DecimalField(default=0, editable=False, max_digits=6, decimal_places=5)),
                ('damped_popularity', models.DecimalField(default=0, editable=False, max_digits=6, decimal_places=5)),
                ('last_featured', models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0), editable=False)),
                ('times_featured', models.IntegerField(default=0, editable=False)),
                ('in_reply_to', models.ForeignKey(related_name='replies', default=None, blank=True, to='ident.Dent', null=True)),
                ('poster', models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
                'ordering': ('-time',),
                'abstract': False,
                'get_latest_by': 'time',
            },
        ),
        migrations.CreateModel(
            name='PrivateDent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=1024)),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('poster', models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True)),
                ('rec_user', models.ForeignKey(related_name='received_privatedent_set', verbose_name=b'Recipient', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-time',),
                'get_latest_by': 'time',
                'permissions': (('can_use_privatedents', 'Can use StalkMail'),),
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email_is_public', models.BooleanField(default=False, help_text='Display e-mail publicly.', verbose_name=b'E-mail')),
                ('i2pbote', models.CharField(help_text='Your <a href="http://i2pbote.i2p/">i2pbote</a> address (publicly shown).', max_length=3000, blank=True)),
                ('i2pmessenger', models.CharField(help_text='Your <a href="http://echelon.i2p/qti2pmessenger/">i2pmessenger</a> address (publicly shown).', max_length=512, verbose_name=b'I2Pmsn', blank=True)),
                ('nightweb', models.CharField(help_text='Your <a href="https://nightweb.net/">Nightweb</a> userhash (publicly shown).', max_length=512, blank=True)),
                ('description', models.CharField(help_text='A brief, public, description of yourself.', max_length=1024, blank=True)),
                ('homepage', models.CharField(help_text='Your Website (publicly shown).', max_length=250, verbose_name=b'WWW', blank=True)),
                ('avatar', ident.user.models.IdentStdImageField(help_text='Image in png or jpg format, will be scaled down to max. 150x200 pixels. Square images preferred for nice small thumbnails. Leave this empty to keep the current unchanged.', upload_to=b'avatars', verbose_name=b'avatard', blank=True)),
                ('karma', models.SmallIntegerField(default=0, help_text=b'Your karma', editable=False, blank=True)),
                ('subscriptions', models.ManyToManyField(related_name='followers', to=settings.AUTH_USER_MODEL, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
                'ordering': ['user'],
                'permissions': (('new_user_restrictions_lifted', 'New user restrictions lifted'),),
            },
        ),
        migrations.CreateModel(
            name='UserSiteSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', ident.dent.fields.LanguageField(default=b'en', help_text='The language to use when displaying the site. Your dents will also be tagged as this language when their content language cannot be guessed.', max_length=7, blank=True, choices=[(b'af', b'Afrikaans'), (b'ar', b'Arabic'), (b'ast', b'Asturian'), (b'az', b'Azerbaijani'), (b'bg', b'Bulgarian'), (b'be', b'Belarusian'), (b'bn', b'Bengali'), (b'br', b'Breton'), (b'bs', b'Bosnian'), (b'ca', b'Catalan'), (b'cs', b'Czech'), (b'cy', b'Welsh'), (b'da', b'Danish'), (b'de', b'German'), (b'el', b'Greek'), (b'en', b'English'), (b'en-au', b'Australian English'), (b'en-gb', b'British English'), (b'eo', b'Esperanto'), (b'es', b'Spanish'), (b'es-ar', b'Argentinian Spanish'), (b'es-mx', b'Mexican Spanish'), (b'es-ni', b'Nicaraguan Spanish'), (b'es-ve', b'Venezuelan Spanish'), (b'et', b'Estonian'), (b'eu', b'Basque'), (b'fa', b'Persian'), (b'fi', b'Finnish'), (b'fr', b'French'), (b'fy', b'Frisian'), (b'ga', b'Irish'), (b'gl', b'Galician'), (b'he', b'Hebrew'), (b'hi', b'Hindi'), (b'hr', b'Croatian'), (b'hu', b'Hungarian'), (b'ia', b'Interlingua'), (b'id', b'Indonesian'), (b'io', b'Ido'), (b'is', b'Icelandic'), (b'it', b'Italian'), (b'ja', b'Japanese'), (b'ka', b'Georgian'), (b'kk', b'Kazakh'), (b'km', b'Khmer'), (b'kn', b'Kannada'), (b'ko', b'Korean'), (b'lb', b'Luxembourgish'), (b'lt', b'Lithuanian'), (b'lv', b'Latvian'), (b'mk', b'Macedonian'), (b'ml', b'Malayalam'), (b'mn', b'Mongolian'), (b'mr', b'Marathi'), (b'my', b'Burmese'), (b'nb', b'Norwegian Bokmal'), (b'ne', b'Nepali'), (b'nl', b'Dutch'), (b'nn', b'Norwegian Nynorsk'), (b'os', b'Ossetic'), (b'pa', b'Punjabi'), (b'pl', b'Polish'), (b'pt', b'Portuguese'), (b'pt-br', b'Brazilian Portuguese'), (b'ro', b'Romanian'), (b'ru', b'Russian'), (b'sk', b'Slovak'), (b'sl', b'Slovenian'), (b'sq', b'Albanian'), (b'sr', b'Serbian'), (b'sr-latn', b'Serbian Latin'), (b'sv', b'Swedish'), (b'sw', b'Swahili'), (b'ta', b'Tamil'), (b'te', b'Telugu'), (b'th', b'Thai'), (b'tr', b'Turkish'), (b'tt', b'Tatar'), (b'udm', b'Udmurt'), (b'uk', b'Ukrainian'), (b'ur', b'Urdu'), (b'vi', b'Vietnamese'), (b'zh-cn', b'Simplified Chinese'), (b'zh-hans', b'Simplified Chinese'), (b'zh-hant', b'Traditional Chinese'), (b'zh-tw', b'Traditional Chinese')])),
                ('index_page', models.CharField(default=b'homepage', help_text='The page to use as the site index while logged in.', max_length=12, choices=[(b'homepage', 'Freshest Dents'), (b'subscribed', 'Your Stalk-o-vision'), (b'userpage', 'Your User Page')])),
                ('session_timeout', models.PositiveSmallIntegerField(default=None, help_text='Max site inactivity (mins) before auto-logout (0 for never).', null=True, blank=True)),
                ('theme', models.CharField(default=b'burgundy', help_text=b'The site theme to use.', max_length=12, choices=[(b'burgundy', b'Burgundy'), (b'corporat', b'Corporat')])),
                ('user_css', models.TextField(default=b'', help_text='Additional CSS which will be applied to all pages in addition to the site CSS, use this to customize ID3NT to your liking.', blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(auto_now=True)),
                ('value', models.IntegerField(default=1, help_text=b'+1 for up and -1 for downvoting')),
                ('dent', models.ForeignKey(to='ident.Dent', blank=True)),
                ('voter', models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
                'ordering': ('dent', 'voter', 'value'),
                'permissions': (('can_vote_up', 'Can vote up'), ('can_vote_down', 'Can vote down')),
            },
        ),
        migrations.AddField(
            model_name='dent',
            name='votes',
            field=models.ManyToManyField(related_name='votes', through='ident.Vote', to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
