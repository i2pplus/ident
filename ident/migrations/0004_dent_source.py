# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ident', '0003_update_profile'),
    ]

    operations = [
        migrations.AddField(
            model_name='dent',
            name='source',
            field=models.CharField(default=b'Web', max_length=140),
        ),
    ]
