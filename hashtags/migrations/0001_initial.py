# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hashtag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.SlugField(unique=True, max_length=64, verbose_name='name')),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'hashtag',
                'verbose_name_plural': 'hashtags',
            },
        ),
        migrations.CreateModel(
            name='HashtaggedItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('hashtag', models.ForeignKey(to='hashtags.Hashtag')),
            ],
            options={
                'verbose_name': 'hashtagged item',
                'verbose_name_plural': 'hashtagged items',
            },
        ),
        migrations.AlterUniqueTogether(
            name='hashtaggeditem',
            unique_together=set([('hashtag', 'content_type', 'object_id')]),
        ),
    ]
