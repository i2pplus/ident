## Requirements

 * Django==1.8.4
 * MySQL-python==1.2.5
 * Pillow==2.9.0
 * flup==1.0.3.dev-20110405
 * matplotlib==1.4.3
 * pytz==2015.4
 * django-appconf==0.6
 * django-extensions==1.0.3
 * --allow-external django-contrib-requestprovider
 * --allow-unverified django-contrib-requestprovider
 * django-contrib-requestprovider==1.0.1
 * django-messages-extends==0.4
 * pyenchant==1.6.6
 * guess_language-spirit==0.5.2
 * django-nuages-tag==0.1.3
 * django-split-settings==0.1.2
 * djangorestframework==3.2.3
 * djangorestframework-oauth==1.0.1
 