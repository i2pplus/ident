from django.db.models.signals import pre_delete, pre_save, post_save
from reputation.models import Reputation


class BaseReputationHandler(object):
    """
    Default handler for creating ReputationHandler objects.
    """
    
    def __init__(self):
        pre_save.connect(self._pre_save_signal_callback, sender=self.model, weak=False)
        post_save.connect(self._post_save_signal_callback, sender=self.model, weak=False)
        pre_delete.connect(self._pre_delete_signal_callback, sender=self.model, weak=False)

    def _pre_save_signal_callback(self, **kwargs):
        sender = kwargs['sender']
        instance = kwargs['instance']
        try:
            current = sender.objects.get(pk=instance.pk)
            if self.has_changed(current, instance):
                self.modify_reputation(instance)
        except sender.DoesNotExist:
            pass

    def _post_save_signal_callback(self, **kwargs):
        instance = kwargs['instance']
        created = kwargs['created']
        if created:
            self.modify_reputation(instance)

    def _pre_delete_signal_callback(self, **kwargs):
        sender = kwargs['sender']
        instance = kwargs['instance']
        self.remove_reputation(instance)

    def check_conditions(self, instance):
        return False

    def has_changed(self, current, instance):
        return False

    def remove_on_delete(self, instance):
        return False

    def get_target_object(self, instance):
        pass

    def get_target_user(self, instance):
        pass

    def get_originating_user(self, instance):
        pass

    def get_value(self, instance):
        return 0

    def modify_reputation(self, instance):
        if self.check_conditions(instance):
            Reputation.objects.log_reputation_action(user = self.get_target_user(instance), 
                                                     originating_user = self.get_originating_user(instance),
                                                     target_object = self.get_target_object(instance),
                                                     action_value = self.get_value(instance))

    def remove_reputation(self, instance):
        if self.check_conditions(instance) and self.remove_on_delete(instance):
            Reputation.objects.delete_reputation_action(user = self.get_target_user(instance), 
                                                        originating_user = self.get_originating_user(instance),
                                                        target_object = self.get_target_object(instance))
