import urllib
import urllib2
import os

import BeautifulSoup as BS

proxy_port = "4444"

proxy_soup = lambda url: BS.BeautifulSoup(urllib2.build_opener(urllib2.ProxyHandler({"http":"127.0.0.1:%s"%(proxy_port)})).open(url).read())
proxy_post = lambda url, data: urllib2.build_opener(urllib2.ProxyHandler({"http":"127.0.0.1:%s"%(proxy_port)})).open(url, data)

class identAPI:
    def __init__(self):
        self.root_url = "http://id3nt.i2p"
        self.rss_feed = "feed"
        self.create_url = "api/1/d/create/"

    def get_feed(self):
        '''Returns a dictionary representation of dents returned in the rss feed'''
        soup = proxy_soup(os.path.join(self.rooturl, self.rss_feed))
        soup = [s.extract() for s in soup("item")]
        dent_list = {}
        for x in range(len(soup)):
            poster = soup[x].title.text.split("(")[1].replace(")", "").encode()
            dent = soup[x].description.text.encode()
            date = soup[x].pubdate.text.encode()
            link = soup[x].guid.text.encode()
            title = soup[x].title.text.encode()
            dent_list[x] = {"poster":poster, "dent":dent, "date":date, "link":link, "title":title}
        return dent_list

    def get_last(self, n):
        '''Returns a dictionary represenation of the last n dents'''
        soup = proxy_soup(os.path.join(self.root_url, self.rss_feed))
        soup = [s.extract() for s in soup("item")]
        dent_list = {}
        for x in range(n):
            poster = soup[x].title.text.split("(")[1].replace(")", "").encode()
            dent = soup[x].description.text.encode()
            date = soup[x].pubdate.text.encode()
            link = soup[x].guid.text.encode()
            title = soup[x].title.text.encode()
            dent_list[x] = {"poster":poster, "dent":dent, "date":date, "link":link, "title":title}
        return dent_list

    def get_user_dents(self, user):
        '''Returns a dictionary representation of the dent feed for a given user'''
        soup = proxy_soup(os.path.join(self.root_url, "u", user, self.rss_feed))
        soup = [s.extract() for s in soup("item")]
        dent_list = {}
        for x in range(len(soup)):
            poster = soup[x].title.text.split("(")[1].replace(")", "").encode()
            dent = soup[x].description.text.encode()
            date = soup[x].pubdate.text.encode()
            link = soup[x].guid.text.encode()
            title = soup[x].title.text.encode()
            dent_list[x] = {"poster":poster, "dent":dent, "date":date, "link":link, "title":title}
        return dent_list

    def get_last_user_dents(self, user, n):
        '''Returns a dictionary representation of the last n dents by a given user'''
        soup = proxy_soup(os.path.join(self.root_url, "u", user, self.rss_feed))
        soup = [s.extract() for s in soup("item")]
        dent_list = {}
        for x in range(n):
            poster = soup[x].title.text.split("(")[1].replace(")", "").encode()
            dent = soup[x].description.text.encode()
            date = soup[x].pubdate.text.encode()
            link = soup[x].guid.text.encode()
            title = soup[x].title.text.encode()
            dent_list[x] = {"poster":poster, "dent":dent, "date":date, "link":link, "title":title}
        return dent_list

    def dent(self, user, password, text):
        '''Posts a dent under the given user with the text provided'''
        post_data = urllib.urlencode({"user":user, "password": password, "text":text})
        proxy_post(os.path.join(self.root_url, self.create_url), post_data)
        return None
