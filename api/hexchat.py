#
# HexChat id3nt client plugin
# by chisquare
#
import xchat
import os,urllib,urllib2,traceback,threading


def log(msg):
    msg = str(msg)
    for l in msg.split(os.linesep):
        xchat.prnt('[id3nt] %s'%l)

def init():
    if xchat.get_pluginpref('id3nt-addr') is None:
        xchat.set_pluginpref('id3nt-addr','id3nt.i2p')
    if xchat.get_pluginpref('id3nt-i2p-proxy') is None:
        xchat.set_pluginpref('id3nt-i2p-proxy','http://127.0.0.1:4444')
    log('Loaded')

def set_login(text,a,b):

    if len(text) != 3:
        log('ERROR: bad login format')
        return
    xchat.del_pluginpref('id3nt-usr')
    xchat.del_pluginpref('id3nt-password')
    if not xchat.set_pluginpref('id3nt-user',text[1]):
        log('Failed to set login credentials')
        return
    elif not xchat.set_pluginpref('id3nt-password',text[2]):
        log('Failed to set login credentials')
    else:
        log('Login Credentials Set')
        log('username: %s'%xchat.get_pluginpref('id3nt-user'))
        log('password: %s'%xchat.get_pluginpref('id3nt-password'))

def clr_login(a,b,c):
    if xchat.del_pluginpref('id3nt-user') and xchat.del_pluginpref('id3nt-password'):
        log('Cleared Login Credentials')
    else:
        log('Failed to clear login Credentials')


def fork_post(text,a,b):
    t = ''
    for p in text[1:]:
        t += '%s '% p
    def p():
        post(t.strip())
    threading.Thread(target=p,args=()).start()

def post(text):
    '''
    Post status update
    '''

    usr = xchat.get_pluginpref('id3nt-user')
    pswd = xchat.get_pluginpref('id3nt-password')
    prxy = xchat.get_pluginpref('id3nt-i2p-proxy')
    addr = xchat.get_pluginpref('id3nt-addr')
    if prxy is None:
        log('Please make sure id3nt-i2p-proxy is set properly')
    elif addr is None:
        log('Please make sure id3nt-addr is set properly')
    elif usr is None or pswd is None:
        log('Please set login: /id3ntLogin username password')
    else:
        log('Posting Update...')
        opener = urllib2.build_opener(urllib2.ProxyHandler({'http':prxy}))
        opener.http_error_default = lambda req, fp, code, msg, hdrs: log('Error: HTTP %d %s'%(code,msg))
        data = urllib.urlencode({'user':usr,'password':pswd,'text':text})
        try:
            resp = opener.open('http://%s/api/1/d/create/'%addr,data=str(data))
            if resp.code != 200:
                log('Update Failed: HTTP %d'%resp.code)
            else:
                log('Update Posted')
        except urllib2.URLError as e:
            log(e)
        except:
            log(traceback.format_exc())

def prnt_stats(a,b,c):
    for stat in ['user','password','addr','i2p-proxy']:
        stat = 'id3nt-%s'%stat
        log('%s = %s'%(stat,xchat.get_pluginpref(stat)))

def set_prxy(text,a,b):
    if len(text) != 2:
        log('Bad arguments')
    xchat.set_pluginpref('id3nt-i2p-proxy',text[1])

__module_name__ = 'id3nt'
__module_description__ = 'id3nt.i2p hexchat plugin'
__module_version__ = '0.0.1'
xchat.hook_command('id3nt',fork_post,help='Update status on id3nt.i2p')
xchat.hook_command('id3ntCfg',prnt_stats,help='Print Config Values')
xchat.hook_command('id3ntLogin',set_login,help='Set local login credentials to id3nt')
xchat.hook_command('id3ntClear',clr_login,help='Clear local login credentials to id3nt')
xchat.hook_command('id3ntProxy',set_prxy,help='Set i2p http proxy')
init()
