#!/bin/sh
set -e
. ./project.vars

if [ ! $venv ]; then
    echo "ERROR: virtualenv not found!" >&2
else
    if [ ! -d $venv_dir ] ; then
        $venv --distribute $venv_dir
    fi

    . $venv_dir/bin/activate
    # Needs to be done first so that guess-language can use it
    pip install 3to2
    # Needs to be done first so that matplotlib can use it
    pip install numpy
    # Install dependencies
    pip install -r requirements_ident.txt
    # Apply I2P-related patches
    patch -p0 <python-i2p.patch
fi
